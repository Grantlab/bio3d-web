# Bio3D-Web for online interactive analysis of protein structure ensembles #

Bio3D-web is an online application for analyzing the sequence, structure and conformational heterogeneity of protein families. This application facilitates the identification of protein structure sets for analysis, their alignment and refined structure superposition, sequence and structure conservation analysis, mapping and clustering of conformations, and the quantitative comparison of their predicted structural dynamics.

Bio3D-web requires no programming knowledge and thus decreases the entry barrier to performing advanced comparative sequence, structure and dynamics analysis. Bio3D-web is powered by the previously described Bio3D R package for structural bioinformatics ([Grant et al., 2006](http://bioinformatics.oxfordjournals.org/cgi/content/abstract/22/21/2695); [Skjærven et al., 2014](http://www.biomedcentral.com/1471-2105/15/399/abstract)).

Bio3D-web is based on the [Bio3D](http://thegrantlab.org/bio3d/) and [Shiny](https://shiny.rstudio.com/) R packages. 

## Features ##

* Identify and select PDB structures related to your input query protein.
* Align selected PDB structures and explore sequence similarity and conservation.
* Superimpose selected structures and explore their invariant structural core and conformational differences.
* Explore the major conformational features, inter-conformer relationships and structural variability of the selected ensemble.
* Compare the predicted large-scale motions and local flexibility of all user selected structures.

## Online usage

Bio3d-web is available from [bio3d.ucsd.edu](http://bio3d.ucsd.edu/pca-app/).

## Run locally

Bio3D-web is a Shiny app which can be run locally, e.g. from the R console with the command: 

```
#!r
library(shiny)
runApp("./pca-app")
```

**Note**: Bio3d-web has several requirements. See [requirements](https://bitbucket.org/larsss/bio3d-web/wiki/Requirements) page of our [wiki](https://bitbucket.org/larsss/bio3d-web/wiki/Home).  


## Pre-built Docker image ##

By far the most convenient way to install and run Bio3d-web is through a [docker](https://www.docker.com/) container. This facilitates running Bio3D-web locally without installing the extensive set of requirements. Bio3D-web is available as a pre-build docker image available from https://hub.docker.com/r/bio3d/bio3d-web/ . 

```
#!bash
export DATADIR=/tmp/shinydata
export LOGDIR=/tmp/shinylogs

docker pull bio3d/bio3d-web
docker run --rm -p 3838:3838 \
   -v $DATADIR:/net/shiny_data \
   -v $LOGDIR:/var/log/shiny-server \
   bio3d/bio3d-web
```

**Note**: To run a temporary container with Shiny Server hosting Bio3D-web you currently need to expose a directory on the host to the container holding data files with rather generous permissions:

```
#!bash
export DATADIR=/tmp/shinydata
mkdir $DATADIR && chmod -R 777 $DATADIR
```



## Development version in a Docker container ##

Dockerize the latest development version of Bio3D-web (found in this repo) with:

```
#!bash
# Directories on the host
export DATADIR=/tmp/shinydata
export LOGDIR=/tmp/shinylogs

# Build image 
docker build . -t bio3d/bio3d-web:devel -f Dockerfile 

# Run temporary container - expose directories on the host to the container
docker run --rm -p 3838:3838 \
    -v $DATADIR:/net/shiny_data/ \
    -v $LOGDIR:/var/log/shiny-server/ \
    bio3d/bio3d-web:devel
```


## Contributing to Bio3D-web ##

We are always interested in adding additional functionality to Bio3D and Bio3D-web. If you have ideas, suggestions or code that you would like to distribute as part of this package, please contact us (see below). You are also encouraged to contribute your code or issues directly to [this repository](https://bitbucket.org/Grantlab/bio3d/) for incorporation into the development version of the package. For details on how to do this please see the [developer wiki](https://bitbucket.org/Grantlab/bio3d/wiki/Home).


## Citing Bio3D-web ##

Online interactive analysis of protein structure ensembles with Bio3D-web.<br>
Skjærven, Jariwala, Yao, Grant, (2016) *Bioinformatics*. <br>
( [To the journal](http://bioinformatics.oxfordjournals.org/content/early/2016/07/14/bioinformatics.btw482.abstract) ) 


## Contact ##

You are welcome to:

* Submit suggestions and bug-reports at: https://bitbucket.org/larsss/bio3d-web/issues 
* Send a pull request on: https://bitbucket.org/larsss/bio3d-web/bio3d/pull-requests
* Compose a friendly e-mail to: bjgrant@umich.edu
