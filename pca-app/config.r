
configuration <<- list(
    
  debug = FALSE, 
    
  pdbdir = list(
      archive = FALSE,
      rawfiles = paste0(tempdir(), "/rawfiles"),
      splitfiles = paste0(tempdir(), "/splitfiles"),
      biounitfiles = paste0(tempdir(), "/biounitfiles")
    ),
  
  annodir = list(
      archive = FALSE,
      annofiles = paste0(tempdir(), "/annofiles"),
      pfamfiles = paste0(tempdir(), "/pfamfiles")
  ),
  
  user_data = list(
      path = paste0(tempdir(), "/userdata")
  ),
  
  hmmer = list(
    local = FALSE,
    exefile = system('which phmmer', intern=TRUE),
    pdbseq = "/path/to/pdb/seqs/pdb_seqres.txt"
  ),
  
  muscle = list(
      exefile = system('which muscle', intern=TRUE)
  ),
  
  dssp = list(
      exefile = system('which dssp', intern=TRUE)
  ),
  
  snapshot = list(
    rgl = FALSE,
    pymol = Sys.which('pymol')
  )
  
)
