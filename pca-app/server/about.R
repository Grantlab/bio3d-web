
observeEvent(input$page1, {
    updateNavbarPage(session, inputId = 'navbarpage', selected = 'search')
})

output$pdftutorial <- downloadHandler(
    filename = function() {
        'PCA-WebApp.pdf'
    },
    content = function(con) {
        file.copy("PCA-WebApp.pdf", con)
    }
    )

output$demo_kinesin <- renderUI({
    node <- as.character(Sys.info()['nodename'])
    ht <- 'http://'
    if(grepl('dcmb', node)) {
        ht <- 'https://'
    }
    tags$a(href=paste0(ht, node, '/pca-app/?demo=kinesin'),
           target='_blank', 'Kinesin Demo')
})

output$demo_mbp <- renderUI({
    node <- as.character(Sys.info()['nodename'])
    ht <- 'http://'
    if(grepl('dcmb', node)) {
        ht <- 'https://'
    }
    tags$a(href=paste0(ht, node, '/pca-app/?demo=mbp'),
           target='_blank', 'MBP Demo')
})

output$demo_leut <- renderUI({
    node <- as.character(Sys.info()['nodename'])
    ht <- 'http://'
    if(grepl('dcmb', node)) {
        ht <- 'https://'
    }
    tags$a(href=paste0(ht, node, '/pca-app/?demo=leut'),
           target='_blank', 'LeuT Demo')
})

output$demo_adk <- renderUI({
    node <- as.character(Sys.info()['nodename'])
    ht <- 'http://'
    if(grepl('dcmb', node)) {
        ht <- 'https://'
    }
    tags$a(href=paste0(ht, node, '/pca-app/'),
           target='_blank', 'Adk Demo')
})


