#####################
##-- Align tab observers
####################

observeEvent(rv$aligned, {
    if(rv$aligned) {
        cut <- cutree_seqide()
        reset_splitTreeK(cut)
    }
})

observeEvent(input$assembly, {
    if(input$assembly == "bioassembly") {
        rv$bioassembly <- TRUE
    }
    else {
        rv$bioassembly <- FALSE
    }
})


observeEvent(input$selected_pdbids, {
  rv$fitted <- FALSE
  rv$pceed <- FALSE
  if(!length(input$selected_pdbids) > 0) {
    rv$selacc <- get_acc()
  }

  if(!all(rv$selacc %in% input$selected_pdbids))
    rv$selacc <- input$selected_pdbids

  if(!all(input$selected_pdbids %in% rv$selacc))
    rv$selacc <- input$selected_pdbids

  if(slv$status &&
     identical(sort(rv$selacc),
               sort(data$selacc))) {
      if(data$fitted) rv$fitted <- TRUE
      if(data$pceed) rv$pceed <- TRUE
  }
})

observeEvent(input$omit_missing, {
  if(input$navbarpage == 'align') {
  rv$fitted <- FALSE
  rv$pceed <- FALSE
  allacc <- get_acc()

  if(input$omit_missing) {
    pdbs <- align_pdbs()
    ##conn <- inspect.connectivity(pdbs, cut=4.05)
    conn <- inspect_connectivity(pdbs, cut=4.05)
    conn <- sapply(conn, all)

    if(!all(conn)) {

      selacc <- rv$selacc[ rv$selacc %in% pdbs$lab[conn] ]
      rv$selacc <- selacc

      updateSelectInput(session, "selected_pdbids",
                        choices = allacc, selected = selacc)
    }

  }
  else {
    ## default behaviour is to select all ids
    #selacc <- get_restore_state()$selected_pdbids
    #if(!slv$status | is.null(selacc))
    #    selacc <- allacc
    #rv$selacc <- selacc
    rv$selacc <- allacc
    updateSelectInput(session, "selected_pdbids",
                      choices = allacc, selected = allacc)

  }
  if(slv$status &&
     identical(sort(rv$selacc),
               sort(data$selacc))) {
      if(data$fitted) rv$fitted <- TRUE
      if(data$pceed) rv$pceed <- TRUE
  }
  }
})

observeEvent(input$reset_fasta, {
  rv$fitted <- FALSE
  rv$pceed <- FALSE
  rv$selacc <- get_acc()
  acc <- rv$selacc
  updateSelectInput(session, "selected_pdbids",
                    choices = acc, selected = acc)

  updateCheckboxInput(session, "omit_missing", value = FALSE)

  if(slv$status &&
     identical(sort(rv$selacc),
               sort(data$selacc))) {
      if(data$fitted) rv$fitted <- TRUE
      if(data$pceed) rv$pceed <- TRUE
  }
})

observeEvent(input$page3, {
    #save_data()
    updateNavbarPage(session, inputId = 'navbarpage', selected = 'fit')
})

#####################
##-- Align
####################

output$include_hits <- renderUI({

  acc <- get_acc()
  names(acc) <- acc
  if(slv$status & !is.null(state$selected_pdbids)) {
      isolate(selected <- state$selected_pdbids)
  #if(slv$status & !is.null(get_restore_state()$selected_pdbids)) {
  #    isolate(selected <- get_restore_state()$selected_pdbids)
  } else
      selected <- acc
  isolate({
  if(input$omit_missing)
      selected <- intersect(selected, rv$selacc)
  })
  selectInput("selected_pdbids", "Include / exclude hits",
              choices = acc, selected = selected,
              multiple=TRUE)
})

get_acc <- reactive({
      
    if(rv$bioassembly) {
        acc <- get_acc4()
    }
    else {
        acc <- get_acc6()
    }

    isolate({
        if(is.null(rv$selacc))
            rv$selacc <- acc
    })
    return(acc)
})

## returns the accession IDs of selected hits (from the SEARCH tab)
get_acc6 <- reactive({

  row.inds <- sort(as.numeric(input$blast_table_rows_selected))

  message( paste(row.inds, collapse=" ") )

  if(is.null(input$input_type)) return()
  if(input$input_type != "multipdb") {
    blast <- rv$blast
    acc <- blast$acc
    ## user uploaded pdb
    if(input$input_type == 'upload' && (1 %in% row.inds))
        acc <- c(paste0('USER_', get_chainid()), acc)
    acc <- acc[ row.inds ]
  }
  else {
    acc <- get_multipdbids()
  }

  acc <- format_pdbids(acc)
  #isolate({
  #    if(!length(rv$selacc))
  #        rv$selacc <- acc
  #})
  #rv$selacc <- acc

  ## return data$selacc if slv$status ??

  ## this is written as a 'getter', so comment the assignment
  #rv$selacc <- acc
  #isolate({
  #    if(is.null(rv$selacc))
  #        rv$selacc <- acc
  #})

  return(acc)
})

get_acc4 <- reactive({
    acc <- get_acc6()
    return(unique(substr(acc, 1, 4)))
})


## returns the filenames of splitted PDBs
split_pdbs <- reactive({
  message("split__pdbs called")

  ids <- get_acc6()
  if(input$input_type == 'upload')
      ids <- grep('^USER_*', ids, value=TRUE, invert=TRUE)
  unq <- get_acc4()

  ## archive mode - pre splitted PDBs
  if(configuration$pdbdir$archive) {

      files <- format_pdbfilenames(ids, path=configuration$pdbdir$splitfiles, raw=FALSE)
      inds.missing <- which(!file.exists(files))

      if(length(inds.missing) > 0) {
          ## pdb2lum_A.ent.gz --> 2lum_A
          missing_ids <- pdbfilename2label(
              files[ !file.exists(files) ]
          )
          missing <- paste(missing_ids, collapse=", ")
          ##stop(paste("PDB file(s) missing:", missing))
          warning(paste("PDB file(s) missing:", missing))
          
          files <- files[ file.exists(files) ]
          
          if(!length(files) > 0)
              stop("PDB files not found")
      }
  }

  ## download mode - downloads and splits PDBs
  else {
      progress <- shiny::Progress$new(session, min=1, max=length(unq))
      progress$set(message = 'Fetching PDBs',
                   detail = 'Please wait ...')
      
      tryfiles <- paste0(configuration$pdbdir$rawfiles, "/", unq, ".pdb")
      
      if(all(file.exists(tryfiles))) {
          raw.files <- tryfiles
      }
      else {
          raw.files <- vector("character", length(unq))
          for(i in 1:length(unq)) {
              raw.files[i] <- get.pdb(unq[i], path=configuration$pdbdir$rawfiles, gzip=TRUE)
              progress$set(value = i)
          }
          validate(need(!any(raw.files == 1), "PDB not found!"))
      }
      gc()
      progress$close()
      
      progress <- shiny::Progress$new()
      on.exit(progress$close())
      progress$set(message = 'Splitting PDBs',
                   detail = 'Please wait ...',
                   value = 0)
      
      tryfiles <- paste0(configuration$pdbdir$splitfiles, "/", ids, ".pdb")
      if(all(file.exists(tryfiles))) {
          files <- tryfiles
      }
      else {
          files <- pdbsplit(pdb.files=raw.files, ids=ids, overwrite=FALSE,
                            path=configuration$pdbdir$splitfiles,
                            progress=progress)
      }
      gc()
  }
  
  ##progress$close() ## used on.exit()
  ids <- get_acc()
  ids <- ids[grep('^USER_*', ids)]
  if(input$input_type == 'upload' && length(ids) > 0) {
      new.files <- paste0(data_path(), '/', get_pdbid(), '_', get_chainid(), '.pdb')
      if(all(file.exists(new.files)))
          files <- c(new.files, files)
      else
          validate('Please check chain info in uploaded PDB file.')
  }
  
  return(files)
})

## performs the actual alignment of the PDBs
align_pdbs <- reactive({
  input_type <- input$input_type
  #isolate(input_type <- input$input_type)
  #isolate({
  if(is.null(input_type)) return()
  load <- FALSE
  if(slv$status &&
     input_type == state$input_type) {
      if(input_type == 'multipdb')
          load <- TRUE
      else
          load <- identical(sort(input$blast_table_rows_selected),
               sort(state$blast_table_rows_selected))
  }
  if(load) {
    pdbs <- data$pdbs
    selacc <- data$selacc
    rv$selacc <- selacc
    rv$aligned <- TRUE
    session$sendCustomMessage('activeNavs', 'FIT')
    if(state$omit_missing)
        updateCheckboxInput(session, 'omit_missing', value = TRUE)
    #if(!is.null(state$show_alignment_edit))
      #updateCheckboxInput(session, 'show_alignment_edit', value = TRUE)
    return(pdbs)
  }
  #})

  ## check for required PDB files
  get_pdbs()
  files <- split_pdbs()

  progress <- shiny::Progress$new()
  on.exit(progress$close())

  progress$set(message = 'Aligning PDBs',
               detail = 'Please wait ...',
               value = 0)

  isolate({
  if(!input$reset_fasta)
    inc1 <<- 0

  reset <- (input$reset_fasta > inc1)
  inc1 <<- input$reset_fasta
  })
  row.inds <- isolate( sort(as.numeric(input$blast_table_rows_selected)) )
  demo <- ( input_type == 'pdb' & rv$pdbid == '1AKE' &
            rv$chainid == 'A' & isTRUE(all.equal(row.inds, 1:26)) &
            rv$bioassembly == FALSE )

  if(!is.null(input$fastafile_upload) & !reset) {
    infile <- input$fastafile_upload
    aln <- read.fasta(infile$datapath)
    pdbs <- read.fasta.pdb(aln, progress=progress)
  }
  else {
    if(demo)
        pdbs <- readRDS('1AKE_pdbs.RDS')
    else {
        pdbs <- pdbaln(files, verbose=TRUE,
                       exefile=configuration$muscle$exefile,
                       outfile=tempfile(pattern="aln", fileext=".fasta"),
                       progress=progress)

        ## Build bioassemblies
        if(input$assembly == 'bioassembly') {
            progress2 <- shiny::Progress$new()
            on.exit(progress2$close())
            
            progress2$set(message = 'Building biounits',
                         detail = 'Please wait ...',
                         value = 0)

            ## Omit duplicate PDBs, e.g. 1AKE_B
            acc4 <- substr(format_pdbids(pdbfilename2label(basename(pdbs$id))), 1, 4)
            pdbs <- trim(pdbs, row.inds=!duplicated(acc4))

            files <- play.biounit(pdbs, dir.output=configuration$pdbdir$biounitfiles, 
                                  dir.rawpdb=configuration$pdbdir$rawfiles, 
                                  file=tempfile(pattern="aln", fileext=".fasta"),
                                  exefile=configuration$muscle$exefile,
                                  progress=progress2)
            
            progress3 <- shiny::Progress$new()
            on.exit(progress3$close())
            progress3$set(message = 'Aligning biounits',
                          detail = 'Please wait ...',
                          value = 0)
            pdbs <- pdbaln(files, exefile=configuration$muscle$exefile,
                           outfile=tempfile(pattern="aln", fileext=".fasta"),
                           progress=progress3)
        }
    }
  }

  pdbid <- get_pdbid()
  chainid <- get_chainid()
  inds.user <- grep(pattern=pdbid, pdbs$id)

  if(configuration$pdbdir$archive) {
    if(demo) {
      pdbs$lab <- pdbs$id
      ids <- format_pdbids(pdbs$id, tolower)
      pdbs$id <- paste0(configuration$pdbdir$splitfiles, "/",
                    substr(ids, 2, 3), "/pdb", ids, ".ent.gz")
    } else {
    ## pdb2lum_A.ent.gz --> 2lum_A
    pdbs$lab <- pdbfilename2label(pdbs$id)
    }
  }
  else {
    if(demo) {
      pdbs$lab <- pdbs$id
      pdbs$id <- paste0(configuration$pdbdir$splitfiles, '/', pdbs$id, '.pdb')
    } else {
      pdbs$lab <- basename.pdb(pdbs$id)
    }
  }

  ## for bioassembly, use only 4 chars code, omit chain IDs
  pdbs$lab <- format_pdbids(pdbs$lab)
  if(rv$bioassembly) {
      pdbs$lab <- substr(pdbs$lab, 1, 4)
  }

  ## for bioassembly, rename ids to 1AKN_1 for bioassembly 1 etc
  #if(rv$bioassembly) {
  #    acc4 <- substr(pdbs$lab, 1, 4)
  #    acc4 <- paste(acc4, unlist(lapply(table(acc4), function(x) seq(1, x))), sep='_')
  #    pdbs$lab <- acc4
  #}

 rownames(pdbs$ali) <- pdbs$lab
 if(input$input_type == 'upload' && length(inds.user) > 0) {
     pdbs$id[inds.user[1]] <- paste0(data_path(), '/', pdbid, '_', chainid, '.pdb')
     pdbs$lab[inds.user[1]] <- paste0('USER_', chainid)
 }

 gc()
 rv$aligned <- TRUE
 #message(nrow(pdbs$ali))
 if(nrow(pdbs$ali) > 1)
     session$sendCustomMessage('activeNavs', 'FIT')

  if(exists("progress"))
      progress$close()
  if(exists("progress2"))
      progress2$close()
  if(exists("progress3"))
      progress3$close()
  
 return(pdbs)
})

## filters the existing alignment obtained by align_pdbs()
## misleading name, but provides the final alignment
align <- reactive({
  pdbs <- align_pdbs()

  #if(input$omit_missing) {
  #  conn <- inspect.connectivity(pdbs, cut=4.05)
  #  if(!all(conn)) {
  #    labs <- pdbs$lab
  #    pdbs <- trim(pdbs, row.inds=which(conn))
  #    pdbs$lab <- labs[conn]
  #  }
  #}

  selacc <- rv$selacc
  if(length(selacc) > 0) {
    if(length(selacc) != length(pdbs$lab)) {
      ids <- selacc
      labs <- pdbs$lab

      inds <- which(pdbs$lab %in% ids)
      if(length(inds) > 0) {
       pdbs <- trim(pdbs, row.inds = inds)
        pdbs$lab <- labs[inds]
      }
    }
  }

  gc()
  return(pdbs)
})

seqide <- reactive({
  pdbs <- align()
  ide <- seqidentity(pdbs)
  rownames(ide) <- pdbs$lab
  colnames(ide) <- pdbs$lab
  return(ide)
})

seqconserv <- reactive({
  pdbs <- align()
  return(conserv(pdbs$ali, method=input$conserv_method))
})

pfamconserv <- reactive({
  progress <- shiny::Progress$new(session, min=1, max=5)
  on.exit(progress$close())
  progress$set(message = 'Pfam conservation',
               detail = 'Fetching Pfam seed alignment', value = 2)
  pdbs <- align()
  aln_pfam <- get_pfam_seed()
  progress$set(detail = 'Aligning sequences and Pfam seed', value = 3)
  aln <- seq2aln(seq2add = pdbs, aln = aln_pfam, id = pdbs$id,
                 file=file.path(data_path(), 'aln_seed.fa'),
                 exefile=configuration$muscle$exefile)
  ref <- length(aln_pfam$id)+1
  progress$set(detail = 'Calculating conservation', value = 4)
  #cons <- conserv(aln, method = input$conserv_method)
  # Only use PFAM alignment to calculate conservation
  aln2 <- list(id=aln_pfam$id, ali=aln$ali[aln_pfam$id, ,drop=FALSE])
  cons <- conserv(aln2, method = input$conserv_method)

  # set 0 at positions in which number of non-gap alphabets is smaller than 1
  rm.inds <- which(apply(aln2$ali, 2, function(x) sum(!is.gap(x)) < 1))
  cons[rm.inds] <- 0
  cons <- cons[!is.gap(aln$ali[ref,])]
  names(cons) <- pdbs$resno[1, !is.gap(pdbs$resno[1,], c("-", ".", "NA"))]
  return(cons)
})


####################################
####   Alignment output         ####
####################################

check_aln <- reactive({
  aln <- align()
  gaps <- gap.inspect(aln$ali)

  if(!length(gaps$f.inds) > 5) {
    validate("Insufficient non-gap regions in alignment to proceed")
    session$sendCustomMessage('deactivateAllTabs', c('ABOUT', 'SEARCH', 'ALIGN'))
  }

  if(!nrow(aln$ali) > 2) {
      validate( "Insufficient PDBs selected" )
      session$sendCustomMessage('deactivateAllTabs', c('ABOUT', 'SEARCH', 'ALIGN'))
  }
})

output$alignment_summary <- renderUI({
  invisible(capture.output( aln <- align() ))
  check_aln()

  id <- aln$id
  ali <- aln$ali

  nstruct <- length(id)
  dims <- dim(ali)
  gaps <- gap.inspect(ali)
  dims.nongap <- dim(ali[, gaps$f.inds, drop = FALSE])
  dims.gap <- dim(ali[, gaps$t.inds, drop = FALSE])
  #rv$hits_count <- dims[1L]
  isolate(rv$align_summary$seq_rows <- dims[1L])
  isolate(rv$align_summary$pos_cols <- dims[2L])
  isolate(rv$align_summary$pos_nogap <- dims.nongap[2L])
  isolate(rv$align_summary$seq_gap <- dims.gap[2L])

  str <- paste("<strong>Alignment dimensions:</strong><br>",
               paste0("<ul><li>", dims[1L], " sequence rows</li>"),
               paste0("<li>", dims[2L], " position columns</li>"),
               paste0("<li>(", dims.nongap[2L], " non-gap, ", dims.gap[2L], " gap)</li></ul>"),
               sep = "")
  if(dims[1L] <= 15) {
      updateRadioButtons(session, inputId = 'show_alignment', label = 'Show alignment',
                         choices = c('Show' = 'yes', 'Hide' = 'no'),
                         selected = 'yes', inline = TRUE)
  } else {
      updateRadioButtons(session, inputId = 'show_alignment', label = 'Show alignment',
                         choices = c('Show' = 'yes', 'Hide' = 'no'),
                         selected = 'no', inline = TRUE)
  }
  shinyjs::runjs("$(window).scrollTop(0);")
  HTML(str)
})


output$omitted_pdbs_summary <- renderUI({
  invisible(capture.output( pdbs <- align() ))
  check_aln()

  wanted <- get_acc()

  #if(rv$bioassembly) {
  #    wanted <- substr(wanted, 1, 4)
  #    missing <- !unlist(lapply(wanted, function(x) any(grepl(x, pdbs$lab))))
  #}
  #else {
  missing <- !wanted %in% pdbs$lab
  
  if(any(missing))
    HTML("<strong>Omitted PDB(s):</strong>", paste(sort(unique(wanted[missing])), collapse=", "))
})

pdbs_connectivity <- reactive({
  invisible(capture.output( pdbs <- align() ))
  check_aln()

  ids <- pdbs$lab
  nstructs <- length(ids)

  conn <- inspect_connectivity(pdbs, cut=4.05)
  conn <- sapply(conn, all)
  return(conn)
})

output$missres_summary <- renderUI({
  invisible(capture.output( pdbs <- align() ))
  check_aln()

  ids <- pdbs$lab
  nstructs <- length(ids)

  conn <- pdbs_connectivity()
  if(sum(!conn) > 0) {
    inds <- which(!conn)

    str <- paste(paste0("<strong>", sum(!conn), " PDB(s) with missing in-structure residues:</strong><br>"),
                 #"<ul><li>",
                 paste0(ids[!conn], collapse=", "),
                 #"</li></ul>",
                 sep="")
    HTML(str)
  }
  #else {
  #  str <- "No PDBs with missing in-structure residues"
  #}
})

output$npdbs_with_missres <- reactive({
  if(!rv$aligned || input$navbarpage != 'align')
    return(0)

  conn <- pdbs_connectivity()
  return(sum(!conn))
})
outputOptions(output, 'npdbs_with_missres', suspendWhenHidden=FALSE)




output$alignment <- renderUI({
  message("generating HTML")
  shinyjs::runjs('$("input[name=\'show_alignment\']").change(function(){
                 $("html, body").animate({scrollTop:$("#alignment_row").offset().top}, "smooth");
                 });')
  invisible(capture.output( aln <- align() ))

  ali <- aln$ali
  ids <- aln$lab

  tmp1 <- conserv(ali, method="entropy10")
  tmp2 <- conserv(ali, method="identity")
  cons <- rep(" ", ncol(ali))
  cons[ tmp1==1 ] <- "^"
  cons[ tmp2==1 ] <- "*"

  nseq <- length(ids)
  nres <- ncol(ali)
  width <- ifelse(nres > 80, 80, nres)

  block.start <- seq(1, nres, by=width)
  if(nres < width) {
    block.end <- nres
  } else {
    block.end <- unique(c( seq(width, nres, by=width), nres))
  }
  nblocks <- length(block.start)
  bufsize <- nchar(nres)-1


  x <- matrix("", ncol=bufsize + width + bufsize, nrow=nseq)
  block.annot <- x[1,]
  block.annot[c(bufsize + 1, seq(10+bufsize, width+bufsize, by=10)) ] <- "."
  cons.annot <- x[1,]

  progress <- shiny::Progress$new(session, min=1, max=nblocks)
  progress$set(message = 'Generating HTML',
               detail = 'Please wait')


  out <- list()
  for(i in 1:nblocks) {
    x[x!=""] <- ""
    positions <- block.start[i]:block.end[i]
    n <- length(positions)
    aln.inds <- (bufsize + 1):(n + bufsize)
    buf.inds1 <- 1:bufsize
    buf.inds2 <- (1 + bufsize + n):(2 * bufsize + n)

    if(n < (ncol(x) - 2 * bufsize)) {
      x <- x[, 1:(n + 2 * bufsize), drop=FALSE]
      block.annot  <- block.annot[1:(n + 2 * bufsize)]
      cons.annot  <- block.annot[1:(n + 2 * bufsize)]
    }
    ##x[, aln.inds] <- ali[, positions, drop=FALSE]

    x.mat <- abind(x, x, along=3)
    x.mat[, aln.inds, 1] <- ali[, positions, drop=FALSE]
    x.mat[, aln.inds, 2] <- aln$resno[, positions, drop=FALSE]

    annot <-  block.annot
    annot[aln.inds[1]] <- block.start[i]
    annot[aln.inds[length(aln.inds)]] <- block.end[i]

    annot2 <- cons.annot
    annot2[aln.inds] <- cons[positions]
    annot2[buf.inds1] <- ""
    annot2[buf.inds2] <- ""

    m <- buf.inds1[bufsize]+1
    spl <- unlist(strsplit(annot[m], ""))
    for(k in 1:length(spl))
      annot[m-k+1] <- spl[length(spl)-k+1]

    m <- buf.inds2[1]-1
    spl <- unlist(strsplit(annot[m], ""))
    for(k in 1:length(spl))
      annot[m+k-1] <- spl[k]


    ## annotation row: numbering
    tmp <- list()
    tmp[[1]] <- span(
      span(" ", class="aln_id"),
      lapply(annot[buf.inds1],     function(x) span(x, class="aln_buff")),
      lapply(annot[aln.inds], function(x) span(x, class="aln_number")),
      lapply(annot[buf.inds2], function(x) span(x, class="aln_buff")),
      class="aln_row"
      )

    ## sequence row
    tmp.j <- prep.seq.row(tmp, i, x, x.mat, ids, buf.inds1, buf.inds2, aln.inds, width)
    tmp <- tmp.j$tmp
    j <- tmp.j$j
    rm(tmp.j)

    ## annotation row: conservation
    tmp[[j+2]] <- span(
      span(" ", class="aln_id"),
      lapply(annot2[buf.inds1],     function(x) span(x, class="aln_buff")),
      lapply(annot2[aln.inds], function(x) span(x, class="aln_conserv")),
      lapply(annot2[buf.inds2], function(x) span(x, class="aln_buff")),
      class="aln_row"
      )

    out[[i]] <- span(tmp, class="aln_block")
    progress$set(value = i)
  }
  progress$close()
  gc()
  #cat(as.character(out))
  pre(class="alignment",
      out
      )
})

####################################
####  Non-reactive functions    ####
####################################

`prep.seq.row` <- function(tmp, i, x, x.mat, ids, buf.inds1, buf.inds2, aln.inds, width){
    if( length(ids)<50 ) {
        for(j in 1:nrow(x)) {
          tmp[[j+1]] <- span(
            span(ids[j], class="aln_id"),
            lapply(x[j, buf.inds1], function(x) span(x, class="aln_buff", class=x)),
            lapply(seq_along(1:length(x.mat[j, aln.inds,1])), function(x) span(x.mat[j,aln.inds,1][x], class="aln_aminoacid", class=x.mat[j,aln.inds,1][x]
                , "title"="",
                "data-original-title"="Residue info","data-toggle"="popover",
                "data-content"=paste0(
                    "<table class=\"tb_pop\">
                    <tbody>
                        <tr><td>PDB ID:</td><td>",
                            ids[j],"</td></tr>",
                        "<tr><td>PDB residue number:</td><td>",
                            x.mat[j,aln.inds,2][x],"</td></tr>
                        <tr><td>Position in alignment:</td><td>",
                        j,", ",((i-1)*width)+x,"</td></tr>",
                    "</tbody>
                    </table>"
                )
                               )),
            lapply(x[j, buf.inds2], function(x) span(x, class="aln_buff", class=x)),
            class="aln_row"
            )
        }
    } else {
        for(j in 1:nrow(x)) {
            tmp[[j+1]] <- span(
                span(ids[j], class="aln_id"),
                lapply(x[j, buf.inds1], function(x) span(x, class="aln_buff", class=x)),
                lapply(seq_along(1:length(x.mat[j, aln.inds,1])), function(x) span(x.mat[j, aln.inds,1][x], class="aln_aminoacid", class=x.mat[j,aln.inds,1][x])),
                lapply(x[j, buf.inds2], function(x) span(x, class="aln_buff", class=x)),
                class="aln_row"
            )
        }
    }
    return(list(tmp=tmp, j=j))
}


####################################
####   Clustering functions     ####
####################################

hclust_seqide <- reactive({
  pdbs <- align()
  check_aln()
  ide <- seqide()
  rownames(ide) <- pdbs$lab
  colnames(ide) <- pdbs$lab

  d <- as.dist(1-ide)
  h <- hclust(d, method=input$hclustMethod)
  h$labels <- pdbs$lab

  return(h)
})

cutree_seqide <- reactive({
  hc <- hclust_seqide()
  #if( is.null(input$splitTreeK) || (as.numeric(input$splitTreeK) > length(rv$selacc)) || is.null(isolate(rv$align_summary$splitTreeK)))
  if( is.null(input$splitTreeK) || (as.numeric(input$splitTreeK) > length(rv$selacc)) )
    k <- NA
  else
    k <- as.numeric(input$splitTreeK)

  cut <- cutreeBio3d(hc, minDistance=minBranchDist(), k=k)

  ## This shouldn't be here; think where and when we need to
  ## reset the inputSlider('splitTreeK')
  #reset_splitTreeK(cut)

  return(cut)
})

observeEvent(input$setk, {
  hc <- hclust_seqide()
  cut <- cutreeBio3d(hc, minDistance=minBranchDist(), k=NA)
  updateSliderInput(session, "splitTreeK", value = cut$autok)
})

output$kslider <- renderUI({
  isolate({
    cut <- cutree_seqide()
    if(slv$status) {
      cut_load <- state$splitTreeK
      #cut_load <- get_restore_state()$splitTreeK
      if(!is.null(cut_load))
        cut$k <- cut_load
    }
  })

  sliderInput("splitTreeK", "Cluster/partition into K groups:",
              min = 1,
              max = min(length(isolate(rv$selacc)), 10),
              value = cut$k,
              step=1)
})

output$min_branch_dist <- renderUI({
    hc <- hclust_seqide()
    numericInput("minDistance","Minimum branch gap for K autoset:",
                 value = round(hc$height[which.max(diff(hc$height))] * 0.3, 2),
                 step = 0.05, max = max(hc$height))
})

minBranchDist <- function() {
  # Returns the minimum branch distance, used to cut seqide dendrogram.
  #
  # Args:
  #   none
  #
  # Returns:
  #   A numeric, minimum branch distance. When ALIGN tab is visited the first time,
  #   a hclust object obtained from align.R/hclust_seqide() is used.
  #   Else, input$minDistance is returned.
  hc <- hclust_seqide()
  if(is.null(input$minDistance))
      round(hc$height[which.max(diff(hc$height))] * 0.3, 2)
  else
      input$minDistance
}

reset_splitTreeK <- function(cut = NULL) {
  # Resets the sliderInput('splitTreeK') when input is changed.
  #
  # Args:
  #   cut: output from utils.R/cutreeBio3d().
  #
  # Returns:
  #   Called for its effect.

  #if(!is.null(input$splitTreeK) && is.null(isolate(rv$align_summary$splitTreeK))) {
  if(!is.null(input$splitTreeK)) {
    #if(as.numeric(input$splitTreeK) != cut$autok) {
      updateSliderInput(session, "splitTreeK", value = cut$autok,
                        max = min(length(isolate(rv$selacc)), 10))
    #}
    #isolate(rv$align_summary$splitTreeK <- cut$autok)
  }
}

####################################
####     Plotting functions     ####
####################################

output$schematic_alignment <- renderPlot({
    generate_schematic_alignment_plot()
})

generate_schematic_alignment_plot <- function() {
  aln <- align()
  check_aln()

  plot.fasta(aln, hc=input$cluster_alignment, labels=aln$lab,
             main=quote(bold("Sequence Alignment Overview")))
  
}

make.plot.seqide.heatmap <- function() {
  pdbs <- align()
  check_aln()
  ide <- seqide()
  rownames(ide) <- pdbs$lab
  colnames(ide) <- pdbs$lab

  hc <- hclust_seqide()
  grps <- cutree_seqide()$grps

  mar <- as.numeric(c(input$margins0, input$margins0))
  plot1 <- heatmap(1-ide,
                   hclustfun = function(x) {
                     hclust(x, method=input$hclustMethod)
                   },
                   distfun=as.dist, symm=TRUE,
          ColSideColors=as.character(grps),
          RowSideColors=as.character(grps),
          cexRow=input$cex0, cexCol=input$cex0,
          margins=mar
          )
  return(plot1)
}

output$seqide_heatmap <- renderPlot({
  print(make.plot.seqide.heatmap())
})

make.plot.seqide.dendrogram <- function() {
  pdbs <- align()
  check_aln()
  ide <- seqide()

  mar <- c(input$margins0, 5, 3, 1)
  hc <- hclust_seqide()
  cut <- cutree_seqide()

  if(vec_is_sorted(hc$height)) {
    max_branch_gap <- max(diff(hc$height))
    k <- length(unique(cut$grps))

    hclustplot(hc, k=k, labels=pdbs$lab, cex=input$cex0,
               ylab="Identity distance", main="Sequence identity clustering",
               fillbox=FALSE, mar = mar)

    #par("lty" = 2); par("lwd" = 2);
    #rect.hclust(hc, k=k, cluster = cut$grps, border = "grey50")

    ## red line only if auto k
    if(max_branch_gap >= minBranchDist() & cut$autok == cut$k ) {
      abline(h = cut$h, col="red", lty=2)
    }
  } else {
    plot(hc, main="", xlab="", sub="")
  }
}

output$seqide_dendrogram <- renderPlot({
  print(make.plot.seqide.dendrogram())
})


make.plot.conservation <- function() {
  pdbs <- align()
  check_aln()
#  sse <- pdbs2sse(pdbs, ind=1, rm.gaps=FALSE, exefile=configuration$dssp$exefile)
  sse <- list(sse=pdbs$sse[1, !is.gap(pdbs$ali[1, ])])
  class(sse) <- 'sse'
  acc <- get_acc()
  x <- seqconserv()
  x <- x[!is.gap(pdbs$ali[1, ])]
  ylab <- paste("Sequence conservation (", input$conserv_method, ")", sep='')

  ##mar <- as.numeric(c(input$margins02, input$margins02))
  p1 <- plot.bio3d(x, pdbs$resno[1, !is.gap(pdbs$ali[1, ])], sse = sse,
                   ylab=ylab, xlab=paste0('Residue number (reference PDB: ', acc[1], ')'))
                   #cex=input$cex0)

  return(p1)
}

output$conservation <- renderPlot({
  print(make.plot.conservation())
})


make.plot.pfamconservation <- function() {
  pdbs <- align()
  cons <- pfamconserv()
  acc <- get_acc()
  sse <- list(sse = pdbs$sse[1, !is.gap(pdbs$ali[1, ])])
  class(sse) <- 'sse'
  xlab <- paste0('Residue number (reference PDB: ', acc[1], ')')
  ylab <- paste0('Sequence conservation (', input$conserv_method, ')')
  plot1 <- plot.bio3d(cons, names(cons), sse = sse,
                      xlab = xlab, ylab = ylab, cex = input$cex02)
  return(plot1)
}

output$pfam_conservation <- renderPlot({
    print(make.plot.pfamconservation())
})



####################################
####     Download functions     ####
####################################

aln2file <- reactive({
  path <- data_path()
  aln <- align()
  fn <- paste0(path, '/aln.fasta')
  write.fasta(aln, file=fn)
  return(fn)
})

output$fastafile = downloadHandler(
  filename = 'aln.fasta.zip',
  content = function(file) {
    zip(file, files=aln2file(), flags = "-9Xj")
  })

seqide2txt <- reactive({
  path <- data_path()
  pdbs <- align()
  ide <- round(seqide(), 2)

  file <- paste0(path, "/", "seqide.dat")
  write.table(ide, file=file, quote=FALSE)
  return(file)
})

output$seqideZIP = downloadHandler(
  filename = 'seqide.zip',
  content = function(file) {
    zip(file, files=seqide2txt(), flags = "-9Xj")
  })

output$seqide_heatmap2pdf = downloadHandler(
  filename = "ide_heatmap.pdf",
  content = function(FILE=NULL) {
    pdf(file=FILE, onefile=T, width=input$width0, height=input$height0)
    print(make.plot.seqide.heatmap())
    dev.off()
})

output$seqide_dendrogram2pdf = downloadHandler(
  filename = "seqide_dendrogram.pdf",
  content = function(FILE=NULL) {
    pdf(file=FILE, onefile=T, width=input$width0, height=input$height0)
    print(make.plot.seqide.dendrogram())
    dev.off()
  })

output$conservation2pdf = downloadHandler(
  filename = "seqconserv.pdf",
  content = function(FILE=NULL) {
    pdf(file=FILE, onefile=T, width=input$width02, height=input$height02)
    print(make.plot.conservation())
    dev.off()
})

####################################
####        Miscellaneous       ####
####################################

output$copy_url_align <- renderUI({
    bsButton(inputId = 'btn_copy_align', label = 'Copy session link',
             class="btn btn-default action-button btn-copy",
             "data-clipboard-text" = get_url()
             )
})

observe({
    if(is.null(input$btn_copy_align) || input$btn_copy_align == 0) {
        shinyBS::addTooltip(session, id = 'btn_copy_align',
                            title = 'Copied!', placement = 'bottom',
                            trigger = 'manual',
                            options = list(`data-toggle` = 'tooltip'))
    }
})
