
###########################
##-- Sets reactive values for the app
###########################

### set default vaules
rv <- reactiveValues()
rv$input_type <- "pdb"

## input = pdb
if(!is.null(data) &&
   state$input_type == 'pdb') {
  rv$pdbid <- data$pdbid
  rv$chainid <- data$chainid
} else {
  rv$pdbid <- "1AKE"
  rv$chainid <- "A"

}
rv$blast <- readRDS("1AKE_blast.RDS")
rv$limit_hits <- 30
rv$cutoff <- 457

## input = sequence
if(!is.null(data) &&
   state$input_type == 'sequence') {
  rv$sequence <- data$sequence
  rv$tophit <- data$tophit
} else {
  rv$sequence <- "MQYKLVINGKTLKGETTTKAVDAETAEKAFKQYANDNGVDGVWTYDDATKTFTVTE"
  rv$tophit <- "1P7E_A"
}

## input = multi pdb
if(!is.null(data) &&
   state$input_type == 'multipdb') {
  rv$pdb_codes <- data$pdb_codes
  rv$pdbids <- data$pdbids
} else {
  rv$pdb_codes <- "1TND, 1KJY_A"
  rv$pdbids <- c("1TND_A", "1TND_B", "1TND_C", "1KJY_A")
}

## for subsequent tabs
rv$aligned <- FALSE
rv$fitted <- FALSE
rv$pceed <- FALSE
rv$modes <- NULL

## biounits
rv$bioassembly <- FALSE

## for pca_pdbs dataTable
rv$pca_selected <- vector()

## for eNMA filtering
rv$nma_selected <- NULL

## used for hiding stuff on the blast tab
#rv$hits_found <- TRUE

## selected accession ids
rv$selacc <- NULL

## pdbs object for nma calculations
rv$pdbs4nma <- NULL

## Testing variables to be used in report
rv$pdb_summary <- list()
rv$seq_summary <- list()
rv$search_summary <- list()
rv$blast_df_selected <- NULL
rv$align_summary <- list()
rv$fit_summary <- list()
rv$pca_summary <- list()

## reactiveValue for save/restore
#slv <- reactiveValues()
#slv$status <- FALSE ## TRUE for restoring state
#slv$input_type <- 'pdb'

#data <- NULL
#state <- NULL

