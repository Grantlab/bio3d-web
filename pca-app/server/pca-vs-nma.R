
#nma3 <- reactive({
#  if(input$rm_gaps == TRUE) {
#    modes <- nma2()
#  }
#  else {
#    pdbs <- refit()
#
#    progress <- shiny::Progress$new()
#    on.exit(progress$close())
#
#    progress$set(message = 'Calculating normal modes',
#                 detail = 'Please wait',
#                 value = 0)
#    modes <- nma(pdbs, fit=TRUE, rm.gaps=TRUE, progress=progress)
#  }
#  return(modes)
#})

## find the structure with highest overlap with PC-1
best_struct <- reactive({
    pdbs <- rv$pdbs4nma
    modes <- rv$modes
    
    if(!inherits(modes, "enma") | !inherits(pdbs, "pdbs"))
        return(NULL)
 
    pc <- pca(pdbs)
    rvals <- rep(0, length(pdbs$id))

    for(i in 1:length(pdbs$id)) {
        r <- rmsip(pc$U, modes$U.subspace[,, i],
                   subset=5,
                   row.name="PC", col.name="NM")
        rvals[i] = max(r$overlap)
    }

    sel <- pdbs$lab[which.max(rvals)]
    return(sel)
})

pcanma_rmsip <- reactive({
    pdbs <- rv$pdbs4nma
    modes <- rv$modes
    
    if(!inherits(modes, "enma") | !inherits(pdbs, "pdbs"))
        return(NULL)
    
    pc <- pca(pdbs)

    if(is.null(input$viewStruct_rmsip))
        return(NULL)

  r <- rmsip(pc$U, modes$U.subspace[,, as.numeric(input$viewStruct_rmsip)],
             row.name="PC", col.name="NM")
  return(r)
})

output$rmsip_plot3 <- renderPlot({
 make.plot.rmsip()
})

make.plot.rmsip <- function() {
  pdbs <- rv$pdbs4nma
  r <- pcanma_rmsip()

  if(is.null(r))
      return(NULL)
  
  r$overlap <- round(r$overlap, 2)

  #col <- colorRampPalette(c("white", "red"))( 50 )
  cols <- switch(input$overlap_heatmap_color,
                 red = c("white", "red"),
                 blue = c("white", "blue"),
                 yellow = c("white", "yellow", "red"))
  col <- colorRampPalette(cols)(50)

  plot(r, xlab="", ylab="", col=col, axes=FALSE) ## axes is not a graphical arg warning, but works
  box()
  mtext(paste("PC", 1:10, sep="-"), at=1:10, side=1, line=3, las=2, adj=0)
  mtext(paste("NM", 1:10, sep="-"), at=1:10, side=2, line=3, las=1, adj=0)

  maxval <- as.numeric(input$overlap_labels_minval)
  inds <- which(r$overlap >= maxval, arr.ind=T)
  if(nrow(inds)>0) {
    for(i in 1:nrow(inds)) {
      text(inds[i,1], inds[i,2], r$overlap[inds[i,1], inds[i,2]])
    }
  }

  title("NMA-vs-PCA overlap analysis")
  mtext(paste("NMs from PDB ID", pdbs$lab[ as.numeric(input$viewStruct_rmsip)]), line=0.5)

}

output$rmsip_table <- renderDataTable({
  r <- pcanma_rmsip()
  r$overlap <- round(r$overlap, 2)
  rownames(r$overlap) <- paste("PC", 1:ncol(r$overlap), sep="-")
  colnames(r$overlap) <- paste("NM", 1:nrow(r$overlap), sep="-")

  datatable(r$overlap,
            selection = 'none',
            options = list(
              dom = 't',
              scrollX = TRUE,
              ordering = FALSE,
              scrollCollapse = TRUE,
              extensions = 'FixedColumns')
            )
})

output$struct_dropdown_rmsip <- renderUI({
  pdbs <- rv$pdbs4nma
  ids <- 1:length(pdbs$id)
  #sel <- rownames(overlap_summary())[1]
  sel <- which(pdbs$lab %in% best_struct())
  names(ids) <-  pdbs$lab
  selectInput('viewStruct_rmsip', 'Compare NMs of structure:',
              choices=ids, selected=sel)
})



output$rmsipplot2pdf = downloadHandler(
  filename = "nma_rmsip.pdf",
  content = function(FILE=NULL) {
    pdf(file=FILE, width=input$width_overlap_heatmap, height=input$height_overlap_heatmap)
    make.plot.rmsip()
    dev.off()
})

