---
title: "Bio3D-web Summary Report"
author: "Lars Skjaerven, Shashank Jariwala & Barry J. Grant"
date: "`r format(Sys.time(), '%B %d, %Y')` (Report template updated:  April 12, 2016)"
output:
  pdf_document:
    highlight: default
    keep_tex: yes
    number_sections: yes
    toc: yes
    toc_depth: 2
    fig_width: 7
    fig_height: 5
    fig_caption: yes
    includes:
        in_header: header.tex
  md_document:
    fig_height: 7
    fig_width: 5
    fig_caption: yes
    variant: markdown_phpextra
  html_document:
    fig_width: 8
    fig_height: 5
    fig_caption: yes
  word_document:
    fig_caption: yes
    pandoc_args: ["--smart"]
documentclass: article
fontsize: 11pt
geometry: tmargin=2.5cm,bmargin=2.5cm,lmargin=2.5cm,rmargin=2.5cm
linkcolor: blue
bibliography: bio3d.bib
affiliation: University of Bergen (Norway), and University of Michigan (Ann Arbor, US)
---

```{r setup, include = FALSE, echo = FALSE}
options(figcap.prefix = "Figure", figcap.sep = ":", figcap.prefix.highlight = "**",
        tabcap.prefix = "Table", tabcap.sep = ":", tabcap.prefix.highlight = "**")
```

# Overview {.unnumbered}
This report was generated on `r format(Sys.time(), "%A, %B %d %Y at %H:%M (EST)")
` by the *Bio3D principal component analysis and ensemble normal mode analysis web application* ([Bio3D-web](https://dcmb-grant-shiny.umms.med.umich.edu/pca-app)) version **0.1**. For complete version information of all dependencies please see the [session and software version information section](#session-and-software-version-information) below.

All included figures and report values in this document reflect those displayed in the online app with user supplied options (including actual input, graph type, clustering and similarity thresholds etc.). Further customization of analysis protocols and all resulting figures is possible with [Bio3D](http://thegrantlab.org/bio3d/index.php) [@skjaerven15] itself. Please see the [conventional usage example section](#conventional-usage-example) together with our collection of [tutorials](http://thegrantlab.org/bio3d/tutorials) for further details about using Bio3D directly on your own computers.

We also automatically save your data as you proceed through the analysis. To revisit your session, please click the following link:  
`r get_url()`.




# SEARCH: Structure Search Summary


```{r search-pdb-main, eval = (input$input_type == 'pdb'), child = 'pdb.Rmd'}
```

```{r search-seq-main, eval = (input$input_type == 'sequence'), child = 'seq.Rmd'}
```

```{r search-multi-main, eval = (input$input_type == 'multipdb'), child = 'multi.Rmd'}
```


\newpage
\blandscape

```{r table-search, echo=FALSE, results = 'asis'}
## propose to reduce the info (less cols), - BARRY: YES or turn landscape at end of report
## since table in pdf (latex) extends beyond the page-width
## Barry: suggest adding 'Citation'.

if(!report_download) {
    anno <- isolate(rv$blast_df_selected)[isolate(rv$search_summary$selected), ]
    row.names(anno) <- seq(1, nrow(anno)) 
    kable(anno, caption = tabRef('table-search', 'Selected sequence similar structures for further analysis'), col.names=c('ID', 'BitScore', 'eValue', 'Name (PDB Title)', 'Species', 'Ligands', 'Method', 'Resolution (A)', 'Space Group', 'Citation'))
} else {
    anno <- isolate(rv$blast_df_selected)[isolate(rv$search_summary$selected), ]
    anno$eValue <- format(anno$eValue, scientific=TRUE)
    row.names(anno) <- seq(1, nrow(anno)) 
    pander::pandoc.table(anno,
        caption = tabRef('table-search', 'Selected sequence similar structures for further analysis'),
        style = 'rmarkdown',
        justify = c('lrrllllrll'),
        #split.cells = c('5%','5%','25%','20%','15%','10%','5%','15%'),
        split.table = 200
    )
}
```

\elandscape
\newpage


```{r align-main, eval = isolate( rv$aligned ), child = 'align.Rmd'}
```



```{r fit-main, eval = isolate( rv$fitted ), child = 'fit.Rmd'}
```



```{r pca-main, eval = isolate( rv$pceed ), child = 'pca.Rmd'}
```


```{r nma-main, eval = isolate( !is.null(rv$modes) ), child = 'nma.Rmd'}
```

\newpage


# Conventional Usage Example {.unnumbered}

To read your selected input structure (with PDB ID: PDBCODE) into Bio3D directly you can use the following command sequence:

```{r note-readpdb, eval=FALSE}
# NOTE: Replace PDBCODE with your chosen 4 character PDB ID
library(bio3d)
pdb <- read.pdb("PDBCODE")
summary(pdb)
```

To search the online RCSB PDB database with the sequence of your query structure you could use the following commands:

```{r note-blast, eval=FALSE}
# Use hmmer or blast
blast <- blast.pdb(pdbseq(pdb))
hits <- plot(blast)
```

To download and align the identified structures you can use the following commands:

```{r note-align, eval=FALSE}
# Use the optional 'path' input argument to set a specific a download location
files <- get.pdb(hits$pdb.id, split=TRUE)
pdbs <- pdbaln(files)
```

For rigid core identification and structural superposition use:

```{r note-fit, eval=FALSE}
core <- core.find(pdbs)

# Use the optional 'outpath' argument to write superimposed PDBs to disk
xyz <- pdbfit(pdbs, core)
```

Investigate pairwise structural deviations and perform cluster analysis with: 

```{r note-rmsd, eval=FALSE}
rd <- rmsd(xyz)
hc <- hclust(as.dist(rd))
hclustplot(hc, k=2)
```

Perform principal component analysis with:

```{r note-pca, eval=FALSE}
pc <- pca(xyz)
plot(pc)
mktrj(pc)
```

To perform normal mode analysis use:

```{r note-nma, eval=FALSE}
modes <- nma(pdbs)
plot(modes)
mktrj(modes)
```

# Citation information {.unnumbered}
This Bio3D web-app should be referenced with the URL http://thegrantlab.org/bio3d/webapps and the following citation: Skærven, L., Jariwala, S., Yao, X.-Q., & Grant, B. J. (2016). Online interactive analysis of protein structure ensembles with Bio3D-web. Bioinformatics 32(22), 3510—3512. doi: [10.1093/bioinformatics/btw482](http://dx.doi.org/10.1093/bioinformatics/btw482).


# Session and Software Version Information {.unnumbered}
This report was auto-magically generated by [Bio3D](http://thegrantlab.org/bio3d/index.php) along with the additional R packages noted below.  

You can install and run **Bio3D-web** locally by following [these instructions](https://bitbucket.org/Grantlab/bio3d/wiki/How%20to%20install%20Bio3D-web%20locally%20on%20your%20own%20computer).  


```{r, echo=FALSE}
sessionInfo()
```




# References {.unnumbered}
 

