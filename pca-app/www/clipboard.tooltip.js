//$(function(){
$(document).ready(
function () {
  //console.info($('.btn-copy').length);
  var clips = new Clipboard(".btn-copy");
  //console.info('ran the script');
  //console.info(clips);

  clips.on('success', function(e) {
    //console.info('Trigger:', $(e.trigger).text());
    $(e.trigger).tooltip('show');
  });

  // Simplistic detection, from https://github.com/zenorocha/clipboard.js.git
  function fallbackMessage(action) {
      var actionMsg = '';
      var actionKey = (action === 'cut' ? 'X' : 'C');

      if(/iPhone|iPad/i.test(navigator.userAgent)) {
          actionMsg = 'No support :(';
      }
      else if (/Mac/i.test(navigator.userAgent)) {
          actionMsg = 'Press ⌘-' + actionKey + ' to ' + action;
      }
      else {
          actionMsg = 'Press Ctrl-' + actionKey + ' to ' + action;
      }

      return actionMsg;
  }

  clips.on('error', function(e) {
      //var mssg =  fallbackMessage(e.action);
      var mssg = 'Not supported on your browser :( \n Please click again to use Save/Load page.';
      var myTrigger = $(e.trigger);
      myTrigger.attr('title', mssg).tooltip('fixTitle').tooltip('show');
      myTrigger.prop('innerHTML', 'Redirect to Save page');
      myTrigger.click(function() {
          var tab = $('#navbarpage li a[data-value="Save/Load"]');
          tab.click();
      });
  });

  $('body').on('mouseleave', '.btn-copy', function() {
    //$(this).tooltip('hide');
    $('.btn-copy').tooltip('hide');
  });

  //$('.btn-copy').mouseleave(function() {
  //  $(this).tooltip('hide');
  //});

  var scrollNext = function(dest, border) {
    border = typeof border !== 'undefined' ?  border : dest;
    $("html, body").animate({scrollTop:dest.position().top - (0.1 * $(window).height())}, "smooth");
    border.addClass('show-border');
    window.setTimeout(function() {
        border.removeClass('show-border');
    }, 2500);
  }

  $('#page0_ex').click(function() {
      scrollNext($('#example_row'), $('#example_row').find('.well'));
  });

  $('#page0_demos').click(function() {
      scrollNext($('#demo_row'), $('#demo_row').find('.well'));
  });

  $('#page0_help').click(function() {
      scrollNext($('#help_row'), $('#help_row').find('.well'));
  });

  $('#about-more').click(function() {
      scrollNext($('#help_row'), $('#help_row').find('.well'));
  });

  $('#page1_hits').click(function() {
      scrollNext($('#blast_plot_row'), $('#blast_plot_row').find('.well'));
  });
  $('#page1_table').click(function() {
      scrollNext($('#blast_row_table'), $('#blast_table').parent());
  });
  $('#next-btn-align1').click(function() {
      scrollNext($('#seqanalysis_row'), $('#seqanalysis_row').children().find('.well'));
  });
  $('#next-btn-align2').click(function() {
      scrollNext($('#seqcons_row'), $('#seqcons_row').children().find('.well'));
  });
  $('#next-btn-align4').click(function() {
      scrollNext($('#alignment_row'), $('#alignment'));
  });
  $('#next-btn-fit1').click(function() {
      scrollNext($('#structanalysis_row'), $('#structanalysis_row').children().find('.well'));
  });
  $('#next-btn-fit2').click(function() {
      scrollNext($('#rmsf_row'), $('#rmsf_row').children().find('.well'));
  });
  $('#next-btn-pca1').click(function() {
      scrollNext($('#pca_conformer_row'), $('#pca_conformer_row').children().find('.well'));
  });
  $('#next-btn-pca2').click(function() {
      scrollNext($('#pca_contrib_row'), $('#pca_contrib_row').children().find('.well'));
  });
  $('#next-btn-enma1').click(function() {
      scrollNext($('#enma_fluct_row'), $('#enma_fluct_row').children().find('.well'));
  });
  $('#next-btn-enma2').click(function() {
      scrollNext($('#enma_pcanma_row'), $('#enma_pcanma_row').children().find('.well'));
  });
  $('#next-btn-enma4').click(function() {
      scrollNext($('#enma_overlap_row'), $('#enma_overlap_row').children().find('.well'));
  });
  $('#next-btn-enma5').click(function() {
      scrollNext($('#enma_clust_row'), $('#enma_clust_row').children().find('.well'));
  });


  // align tab popover
  $("body").popover({
    html: true,
    selector: ".aln_aminoacid[data-toggle='popover']",
    container: "body",
    title: "Residue info",
    trigger: "hover",
    delay: { "show": 150, "hide": 40 },
    placement: "auto bottom"
  });

});

