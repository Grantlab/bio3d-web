tabPanel(
  "5. eNMA", icon=icon("arrow-right", class = "fa-lg"),
  id = "tab-enma",
  value = 'nma',
  #div(
  #  h3("Normal Mode Analysis", style="border: 1px solid #e3e3e3; border-radius: 4px; padding-top: 10px; padding-bottom: 10px; padding-left: 5px; margin-top: -10px; background-color: white;")
  #  ),

  fluidRow(class = "row-header",
    column(8,
           h3(class = 'h3-title', "Ensemble Normal Mode Analysis"),
           tags$i(id = 'about_nmatab', title = 'About this tab',
                  class = 'fa fa-info-circle fa-2x icon-modal')),
    column(4,uiOutput('copy_url_nma', inline = TRUE))
  ),
  #actionButton3("about_nmatab", "About this tab", icon=icon("comment", class = "fa-lg"), cl="btn btn-warn btn-input action-button", style = "position: fixed; top: 14px; right: 16px; z-index: 2000;"),

  bsModal("modal_nma", "Ensemble Normal Mode Analysis", "about_nmatab", size = "large",
          content=tags$div(

            p(HTML("Normal mode analysis (NMA) characterizes protein dynamics proximal to a local energy minimum. This implies that predictions from NMA approaches can be dependent on the (often arbitrarily chosen) initial structure used for calculations. NMA applied to a single structure can thus be strongly biased to the specific state represented by the structure and may fail to capture the more global picture of structural dynamics (<a href='http://www.ncbi.nlm.nih.gov/pubmed/11287673'>Tama and Sanejouand, 2001</a>). This input structure dependency of NMA also provides an effective way to characterize and potentially classify protein structures via a comparison of NMA derived dynamics. Using this approach additional structural dynamic insight can be obtained that is not readily available from conventional structural comparison. This realization led to our development of the ensemble NMA approach (eNMA), which calculates modes for all structures in a systematic and unified manner.")),

            p(HTML("Critically, the NMA here is performed on all structures in a way that facilities the interpretation of their structural similarity and dissimilarity trends (<a href='http://pubs.acs.org/doi/abs/10.1021/acs.jpcb.6b01991'>Yao et al., 2016</a>). This facilitates the quantitative comparison of normal modes across heterogeneous structures (with unequal sequence composition and length). This also allows a user to more fully explore dynamic trends in selected crystallized states in relation to each other without the conventional caveat of potentially over-interpreting the differences between extreme cases or a single artifactual structure.")),

            p(HTML("Clustering based on calculated eNMA modes and characterization of each cluster (or state) can effectively reveal the dynamical heterogeneity that is missing in single structure based NMA. Indeed, detailed comparison shows that ensemble NMA has an overall improvement over single structure NMA in predicting many important structural dynamic properties (<a href='http://pubs.acs.org/doi/abs/10.1021/acs.jpcb.6b01991'>Yao et al., 2016</a>).")), 

            img(src="./images/enma-clustering.png", style="display: block; margin-left: auto; margin-right: auto;"),

            p(HTML("Furthermore, eNMA can directly reveal state specific trends and differences in residue wise fluctuations of potential functional relevance.")),

             img(src="./images/enma-fluctuations.png", style="display: block; margin-left: auto; margin-right: auto;"),
 
            p(HTML("The actual motions described by each normal mode can be visualized either in the browser, or by downloading a PDB trajectory file or a PyMOL session (.pse) containing a vector field representation. "))
            )
          ),



### Filter structures
  fluidRow(
    column(4,
           wellPanel(
             h4("A) Filter structures"),
             helpText("Focus the structure ensemble by filtering out similar structures to reduce the computational load for NMA. Interactively (de)select structures by clicking the PC conformer plot."),

             selectInput('filter_clusterBy', 'Filter by',
                         choices=list(
                             "PCA Clustering" = "pca",
                             "RMSD Clustering" = "rmsd",
                             "Sequence Clustering" = "seqide"),
                         selected = "pca"),
             uiOutput("filter_rmsdInput"),

             radioButtons("filter_view", "Plotting options",
                                 c("PC conformerplot" = "confplot",
                                   "Dendrogram" = "dendrogram"
                                   ),
                          inline=TRUE),

               conditionalPanel(
                   condition = "input.filter_view == 'confplot'",
                   checkboxInput('label_confplot2', 'Label plot', value=FALSE)
                   ),

               htmlOutput("filter_summary"),
                conditionalPanel(
                    condition = "input.filter_view == 'confplot'",
                    helpText("(Dots highlighted with circles in the PC conformerplot will be included in the calculation).")
                    ),
               conditionalPanel(
                   condition = "input.filter_view == 'dendrogram'",
                   helpText("(PDB IDs colored red in dendrogram will be included in the calculation).")
                   ),
             br(),

             uiOutput("run_enma_btn")

             )
           ),

    column(8,
           conditionalPanel(
               condition = "input.filter_view == 'confplot'",
                plotOutput("pca_confplot2", click = "pca_confplot2_click")
                ),
           conditionalPanel(
               condition = "input.filter_view == 'dendrogram'",
               plotOutput("rmsd_dendrogram2")
               )
           )
    ),





  hr(),
  ## Hide everything below unless normal modes are calculated
  conditionalPanel(
    condition = "output.nmaIsDone",


  ### WebGL visualization
  fluidRow(
           id = "nma_traj_row",
           column(4,
                  wellPanel(
                    bsPopover("popnma1",
                              "Normal mode visualization",
                              "Visualize the normal modes by toggeling the the <b>Show NM Trajectory</b> checkbox. <br><br>Download buttons enable visualization of the motions described by the principal component in external viewers such as PyMOL or VMD.",
                              placement = "right", trigger = "hover",
                              options = list(container = "body")),

                    tags$div(id = "popnma1", icon("info-circle", class = "fa-2x"),
                             style = "position: absolute; right: 25px; top: 5px;"
                             ),


                    h4('B) Normal Modes Visualization'),
                    checkboxInput('show_trj2', 'Show NM Trajectory', value=FALSE),

                    uiOutput('struct_dropdown2'), ## viewStruct_nma
                    selectInput('viewMode_nma', 'Choose Mode:', choices=c(1:10)),


                    sliderInput("mag2", "Magnification factor:",
                                min = 1, max = 20, value = 5),

                     selectInput('viewColor2', 'Color options',
                                 choices=list(
                                   'Displacement Extremes (blue, gray, red)' = 'amalgam',
                                   'Variability Per Position (blue->red)'='mag',
                                   'By Frame (blue->gray->red)'='default'
                               ),
                             selected='amalgam'
                          ),

                    selectInput("viewBGcolor2", "Background color:",
                                c('White'='white', 'Black'='black'),
                                multiple=FALSE),

                    br(),
                    ##actionButton('viewUpdate2', label='Refresh', icon=icon('undo')),
                    downloadButton('nmtraj', label='PDB Trajectory'),
                    br(),
                    actionButton3("next-btn-enma1", "Next (Fluctuations)", icon=icon("arrow-down"), cl="btn btn-primary btn-input action-button", style = "margin-top: 0.9%;")
                    )
                  ),

           column(8,
                  conditionalPanel(
                    condition='input.show_trj2 == true',
                    rglwidgetOutput('nmaWebGL', width = '800px', height = '600px')
                    )
                  )
    ),

         ### Fluctuation plot
         fluidRow(
           id = 'enma_fluct_row',
           column(4,
                  wellPanel(
                    bsPopover("popnma2",
                              "Residue fluctuations",
                              "The fluctuations are calculated based on all 3N-6 normal modes (where N is the number of atoms). Magnitudes should be used by care as normal mode vectors are by definition without magnitude. <br><br>Use option <b>Spread lines</b> to provide fluctuation profiles not on top of eachother.",
                              placement = "right", trigger = "hover",
                              options = list(container = "body")),

                    tags$div(id = "popnma2", icon("info-circle", class = "fa-2x"),
                             style = "position: absolute; right: 25px; top: 5px;"
                             ),



                    h4('C) Residue fluctuations'),

                    checkboxInput('spread', 'Spread lines', value=FALSE),
                    checkboxInput('seqide', 'Sequence conservation', value=FALSE),
                    checkboxInput('labnmafluct', 'Show labels', value=TRUE),
                    ##checkboxInput('signif', 'Show fluct signif', value=FALSE),
                    ##checkboxInput('rm_gaps', 'Omit gap regions', value=TRUE),

                    checkboxInput('cluster', 'Color by clustering', value=TRUE),
                    radioButtons("cluster_by2", "Cluster by",
                                 c(
                                   "PC subspace" = "pc_space",
                                   "RMSIP" = "rmsip",
                                   "RMSD" = "rmsd",
                                   "Sequence" = "sequence"
                                   ),
                                 ##"bhat" = "bhat"),
                                 inline=TRUE),

                    conditionalPanel(
                      condition = "input.cluster_by2 == 'rmsip'",

                      ## K-selecter
                      uiOutput("kslider_rmsip"),
                      actionButton("setk_rmsip", "Auto set number of K groups",
                                   icon=icon("cogs"))
                      ),


                    checkboxInput('show_options3', 'More options', value=FALSE),
                    downloadButton('nmaplot2pdf', "Fluctuations (PDF)"),
                    br(),
                    actionButton3("next-btn-enma2", "Next (PCA-vs-NMA)", icon=icon("arrow-down"), cl="btn btn-primary btn-input action-button", style = "margin-top: 0.9%;")
                    )
                  ),
           column(8,
                    plotOutput("nma_fluctplot")
                  )
           ),

         conditionalPanel(
           condition = "input.show_options3 == true",
           fluidRow(
             column(3,
                    wellPanel(
                      selectInput("hclustMethod_rmsip", label="Clustering method",
                                  choices=list(
                                    "single"="single","complete"="complete","average"="average",
                                    "mcquitty"="mcquitty","median"="median","centroid"="centroid",
                                    "ward.D"="ward.D","ward.D2"="ward.D2"
                                    ),selected="ward.D2"),

                      numericInput("minDistance_rmsip","Minimum branching gap", value = 0.1, step = 0.2)

                      )
                    ),

             column(3,
                    wellPanel(
                      sliderInput("width1", "Figure width (PDF only)",
                                  min = 4, max = 12, value = 7, step=0.5),
                      sliderInput("height1", "Figure height (PDF only)",
                                  min = 4, max = 12, value = 7, step=0.5),
                      uiOutput("nma_fluctplot_ylim")
                      )
                    ),

             column(6,
                    wellPanel(
                      checkboxInput("toggle_all2", "Toggle all", TRUE),
                      uiOutput("checkboxgroup_label_ids2")
                      )
                    )
                 )
           ),




    ### PCA vs NMA (RMSIP)
    fluidRow(
      id = 'enma_pcanma_row',
      hr(),
      column(4,
             wellPanel(
               bsPopover("popnma6",
                         "Compare PCA and NMA",
                         "The normal mode vectors from one PDB structure can be compared with the eigenvectors obtained from principal component analysis (from the PCA tab). <br><br>The values provided in the table to the right correspond to the overlap (i.e. the dot product) between the mode vectors. The RMSIP value provides an overall score for the similarity between the principal components and the normal mode vectors. Orthogonal vectors give the score of 0, while identical vectors give a score of 1. ",
                         placement = "right", trigger = "hover",
                         options = list(container = "body")),

               tags$div(id = "popnma6", icon("info-circle", class = "fa-2x"),
                        style = "position: absolute; right: 25px; top: 5px;"
                        ),

               h4('D) Compare PCA and NMA'),
               helpText("The NMs of the selected 'reference' PDB are compared to the PCs derived from the ensemble of PDB structures. Note: Only the filtered structures are used in the comparison (panel A)."),
               uiOutput('struct_dropdown_rmsip'),

               radioButtons("show_overlap_by", "Show overlap as:",
                            c("Heatmap" = "heatmap",
                              "Table" = "table")),

                conditionalPanel(
                  condition = "input.show_overlap_by == 'heatmap'",
                  sliderInput("overlap_labels_minval", "Show values above:", min=0, max=1, value=0.2, step=0.1),
                  checkboxInput('show_options7', 'More options', value=FALSE),
                  downloadButton('rmsipplot2pdf', "NMA-vs-PCA overlap (PDF)")
                 ),

                 ##br(),
                actionButton3("next-btn-enma4", "Next (Overlap analysis)", icon=icon("arrow-down"), cl="btn btn-primary btn-input action-button", style = "margin-top: 0.9%;")
               )
             ),

      column(8,
             conditionalPanel(
               condition = "output.nmaIsDone",

                conditionalPanel(
                  condition = "input.show_overlap_by == 'heatmap'",
                  plotOutput("rmsip_plot3")
                 ),

                conditionalPanel(
                  condition = "input.show_overlap_by == 'table'",
                  DT::dataTableOutput("rmsip_table")
               )
             )
        )
      ),


         conditionalPanel(
           condition = "input.show_options7 == true",
           fluidRow(
             column(3,
                    wellPanel(
                      sliderInput("width_overlap_heatmap", "Figure width (PDF only)",
                                  min = 4, max = 12, value = 7, step=0.5),
                      sliderInput("height_overlap_heatmap", "Figure height (PDF only)",
                                  min = 4, max = 12, value = 7, step=0.5)
                      )
                    ),

             column(3,
                    wellPanel(
                      radioButtons("overlap_heatmap_color",
                                   "Heatmap coloring",
                                   c("White-red" = "red",
                                     "White-blue" = "blue",
                                     "Heat colors (white-yellow-red)" = "yellow"))
                    )
             )
          )
      ),


             ## Overlap analysis
    fluidRow(
      id = 'enma_overlap_row',
      hr(),
      column(4,
             wellPanel(
               bsPopover("popnma5",
                         "Overlap analysis",
                         "The overlap, or dot product, measures the similarity between a normal mode vector and a vector describing the conformational difference between two structures. An overlap of 1 corresponds to identical vectors, while an overlap of 0 correspond to orthogonal vectors.",
                         placement = "right", trigger = "hover",
                         options = list(container = "body")),

               tags$div(id = "popnma5", icon("info-circle", class = "fa-2x"),
                        style = "position: absolute; right: 25px; top: 5px;"
                        ),

               h4('E) Overlap analysis'),
               helpText("The NMs of the selected 'reference' PDB are compared to the vector describing the difference in conformation between two selected structures (i.e. the reference structure and each of the selected PDBs from the table below)."),
               uiOutput('struct_dropdown_overlap'),
               DT::dataTableOutput('pdbs_table_overlap'),

               checkboxInput('laboverlap', 'Show labels', value=TRUE),
               checkboxInput('coloverlap', 'Color by clustering', value=TRUE),

               downloadButton('overlapplot2pdf', "Overlap (PDF)"),
               br(),
               actionButton3("next-btn-enma5", "Next (Clustering)", icon=icon("arrow-down"), cl="btn btn-primary btn-input action-button", style = "margin-top: 0.9%;")
               )
             ),
      column(8,
             plotOutput("overlap_plot")
             )
      ),


         ## Cluster dendrogram
         fluidRow(
          id = 'enma_clust_row',
           hr(),
           column(4,
                  wellPanel(
                    h4('F) Cluster dendrogram'),
                    checkboxInput('show_confplot2', 'Show PC conformer plot', value=FALSE),
                    
                    radioButtons("color_rmsip_dendrogram_by", "Color by",
                                 c("RMSIP" = "rmsip",
                                   "RMSD" = "rmsd",
                                   "PC distance" = "pc_space"
                                   ),
                                 inline=TRUE),

                    checkboxInput('show_options5', 'More options', value=FALSE),
                    downloadButton('nmadendrogram2pdf', "Dendrogram (PDF)"),
                    br(),
                    actionButton3("page6", "Next (Report)", icon=icon("arrow-right"), cl="btn btn-primary btn-input action-button", style = "margin-top: 0.9%;")

                    )
                  ),
           column(8,
                    plotOutput("dendrogram2")
                  )
           ),

         conditionalPanel(
           condition = "input.show_options5 == true",
           fluidRow(
             column(3,
                    wellPanel(
                      sliderInput("cex2", "Label size",
                                  min = 0.1, max = 3, value = 1, step=0.1),
                      sliderInput("margins2", "Margins",
                                  min = 3, max = 10, value = 5, step=1)

                      )
                    ),

             column(3,
                    wellPanel(
                      sliderInput("width-pcload", "Figure width (PDF only)",
                                  min = 4, max = 12, value = 7, step=0.5),
                      sliderInput("height-pcload", "Figure height (PDF only)",
                                  min = 4, max = 12, value = 7, step=0.5)
                      )
                    )
             )
           ),


         ### Heatmaps
         fluidRow(
           id = 'enma_heat_row1',

           column(12,
                  h4("Cluster heatmaps"),
                  helpText("Row side colors correspond to RMSIP clustering for all plots below. Column side colors correspond to clustering from NMA, FIT, and PCA tab, respectively.")
                  )
           ),

         fluidRow(
           id = 'enma_heat_row2',
           column(4,
                  #conditionalPanel(
                  #  condition = "input.rm_gaps == true",
                    h5("RMSIP heatmap"),
                    plotOutput("rmsip_heatmap2")
                  #  )
                  ),
           column(4,
                  h5("RMSD heatmap"),
                  plotOutput("rmsd_heatmap2")
                  ),
           column(4,
                  h5("PCA heatmap"),
                  plotOutput("pca_heatmap2")
                  )
           ),

          fluidRow(
            column(3,
                   checkboxInput('show_options4', 'More output options', value=FALSE)
                   )
            ),

         conditionalPanel(
           condition = "input.show_options4== true",
           fluidRow(
             column(3,
                    wellPanel(
                      sliderInput("cex3", "Label size",
                                  min = 0.1, max = 3, value = 1, step=0.1),
                      sliderInput("margins3", "Margins",
                                  min = 3, max = 10, value = 5, step=1)
                      )
                    ),
             column(3,
                    wellPanel(
                      sliderInput("width3", "Figure width and height (PDF only)",
                                  min = 4, max = 12, value = 7, step=0.5)
                      )
                    ),
             column(3,
                    wellPanel(
                      downloadButton('nma_rmsip_heatmap2pdf', "RMSIP Heatmap (PDF)"),
                      downloadButton('nma_rmsd_heatmap2pdf', "RMSD Heatmap (PDF)"),
                      downloadButton('pca_heatmap2pdf', "PCA Heatmap (PDF)")
                    )
             )
           )
         )


    )
  )
