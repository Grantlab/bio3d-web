tabPanel('Save/Load', icon = icon('save', class = 'fa-lg'),
    id = 'tab-save',
    tags$script(src = 'session.js'),
    div(
        h3("Save or load your analysis",
            style="border: 1px solid #e3e3e3; border-radius: 4px; padding-top: 10px; padding-bottom: 10px; padding-left: 5px; margin-top: -10px; background-color: white;")
    ),
    fluidRow(
        column(4,
            wellPanel(
                h3('I want to:'),
                radioButtons('slchoice', '',
                             list(`Save current analysis` = 'save',
                                  `Load analysis` = 'load'),
                             inline = F)
            )
        ),
        conditionalPanel(
            condition = "input.slchoice == 'save'",
            column(6,
                h2('Save your analysis'),
                p('The results from your analyses will be saved on the server for 30 days.'),
                htmlOutput('session_url'),
                #p(HTML(paste0('You can revisit your data by clicking this link: ', get_url()))),
                verbatimTextOutput('user_data_path'),
                actionButton3(inputId = "btn_save_data", label = "Save",
                             cl="btn btn-primary btn-input action-button"),
                br(),
                hr(),
                br()#,
                #p('Alternatively, we can email you the session ID. Please enter your email below.'),
                #textInput('user_email', label = 'E-mail:', value = 'user@domain'),
                #actionButton3(inputId = "btn_send_email", label = "Send",
                #             cl="btn btn-primary btn-input action-button")
            )
        ),
        conditionalPanel(
            condition = "input.slchoice == 'load'",
            column(6,
                h2('Load your analysis'),
                p(HTML('You can revisit your data by pasting the session ID in the box below.')),
                textInput('load_data_path', label = 'Session ID:'),
                helpText('Example: 2016-10-01_65d0f48c17e'),
                actionButton3('btn_load_data_path', label = 'Load',
                             cl="btn btn-primary btn-input action-button"),
                br(),
                shinyBS::bsAlert('alert_url')
            )
        )
    )
)
