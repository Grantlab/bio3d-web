context('Basic test of bio3d shiny app')

require(RSelenium)

# Check if Selenium Server standalone is installed.
checkForServer()

# Start Selenium Server and connect to it.
startServer()
remDr <- remoteDriver()
#remDr <- remoteDriver('localhost', port=4444, browserName='firefox', extraCapabilities = list(name='shinytestapp screenshot'))
remDr$open(silent=TRUE)

# Set the URL of bio3d-web
appURL <- 'http://127.0.0.1:1111'

test_that('can connect to app', {

  # Connect to bio3d-web and check if title is correct.
  remDr$navigate(appURL)
  Sys.sleep(20) # time delay for website to response
  appTitle <- remDr$getTitle()[[1]]
  expect_equal(appTitle, 'Bio3D-web')

})

test_that('All tabs are present', {

  # Find all DOM elements associated with the navigation tabs.
  webElems <- remDr$findElements('css selector', '.nav li')

  # Get tab names.
  appTabLabels <- sapply(webElems, function(x) x$getElementText())

  expect_equal(appTabLabels[[1]], '1. SEARCH') 
  expect_equal(appTabLabels[[2]], '2. ALIGN') 
  expect_equal(appTabLabels[[3]], '3. FIT') 
  expect_equal(appTabLabels[[4]], '4. PCA') 
  expect_equal(appTabLabels[[5]], '5. eNMA') 
  expect_equal(appTabLabels[[6]], 'REPORT') 

})

test_that('Input field for PDB ID works properly', {

  # Find the DOM element associated with the input field for PDB ID.
  webElem <- remDr$findElements('css selector', '#pdbid')[[1]]
  expect_equal(webElem$getElementAttribute('type')[[1]], 'text')
  expect_equal(webElem$getElementAttribute('value')[[1]], '1AKE')
  
  # Clear input field and type 1TND (Transducin)
  webElem$clearElement()
  webElem$sendKeysToElement(list('1TND'))

  Sys.sleep(20)
   
  # Find the DOM element associated with the PDB summary. 
  # The summary should be updated along with the input PDB ID.
  webElem <- remDr$findElements('css selector', '#input_pdb_summary')[[1]]
  summary <- webElem$getElementText()[[1]]
  expect_true(grepl('TRANSDUCIN', summary))

  # Find the DOM element associated with the PFAM annotation.
  webElem <- remDr$findElements('css selector', '#pfam_table')[[1]]
  expect_true(grepl('PF00503', webElem$getElementText()[[1]]))

  # Find the DOM element associated with the 2nd ("ID") column of the BLAST table.
  webElems <- remDr$findElements('css selector', '#blast_table tbody tr td:nth-of-type(2)')
  ids <- sapply(webElems, function(x) x$getElementText()[[1]])  
  expect_true(any(grepl('1TND', ids)))
  expect_true(any(grepl('1TAG', ids)))
  expect_true(any(grepl('1TAD', ids)))

})

test_that('bad PDB IDs can be handled properly', {

  # Clear input field and type some crappy PDB ID.
  webElem <- remDr$findElements('css selector', '#pdbid')[[1]]
  webElem$clearElement()
  webElem$sendKeysToElement(list('xxxx'))
 
  Sys.sleep(20)

  # Proper error message should be displayed for the wrong PDB ID.
  webElem1 <- remDr$findElements('css selector', '#input_pdb_summary')[[1]]
  webElem2 <- remDr$findElements('css selector', '#pfam_table')[[1]]
  webElem3 <- remDr$findElements('css selector', '#pdb_chains')[[1]]
  webElem4 <- remDr$findElements('css selector', '#pdbWebGL')[[1]]
  summary <- webElem1$getElementText()[[1]]
  tbl <- webElem2$getElementText()[[1]]
  chns <- webElem3$getElementText()[[1]]
  str <- webElem4$getElementText()[[1]]
  msg <- 'Input PDB code(s) may not be valid.\nPlease check your input.'
  expect_equal(msg, summary)
  expect_equal(msg, chns)
  expect_equal(msg, str)
  expect_true(grepl('Pfam data not found', tbl))

  # Proper error message should be displayed for BLAST output also. 
  webElem1 <- remDr$findElements('css selector', '#cutoff_slider')[[1]]
  webElem2 <- remDr$findElements('css selector', '#hits_slider')[[1]]
  webElem3 <- remDr$findElements('css selector', '#blast_plot')[[1]]
#  webElem4 <- remDr$findElements('css selector', '#blast_table')[[1]]
  cutoff_slider <- webElem1$getElementText()[[1]]
  hits_slider <- webElem2$getElementText()[[1]]
  blast_plot <- webElem3$getElementText()[[1]]
#  blast_tbl <- webElem4$getElementText()[[1]]
  msg <- 'Less than 3 BLAST hits found'
  expect_equal(msg, cutoff_slider)
  expect_equal(msg, hits_slider)
  expect_equal(msg, blast_plot)
#  expect_equal(msg, blast_tbl)

})

remDr$close()
