## dependencies.R  (2016-09-15)
##
## Description: 
##   Used to install R dependencies for Bio3D 'pca-app'
## Usage: 
##   For now just source into your R session.


##- Should we install to user directory 
#local_dir <- Sys.getenv("R_LIBS_USER")
#if (!file.exists(local_dir)) dir.create(local_dir, recursive = TRUE)



##-
##- 1.
##- Install required but missing CRAN packages
##-
cran.pkgs <- c("devtools", "shiny", "RMySQL", "maptools",
               "reshape2", "abind", "threejs", "rjson",
               "shinyBS", "shinyjs", "rgl", "rmarkdown", "knitr",
               "pander")

## Is a CRAN repo set from which to get packages?
cran.repos <- options()$repos
if (is.null(cran.repos)){ cran.repos <- "http://cran.r-project.org" }


get.package <- function(x, cran.repos=cran.repos){
   if (!require(x, character.only = TRUE)){
      install.packages(x, dependencies=TRUE, repos=cran.repos) # type="source")
		#install.packages(x, local_dir, dependencies = TRUE, repos=cran.repos)
      if(!require(x, character.only = TRUE)){
         stop(paste("Dependent package",x,"not found on CRAN:",cran.repos))
      }
   } else {
      cat("Dependent package",x,"already installed\n")
   }
}

##- Install missing 'cran.pkgs' CRAN packages from 'cran.repos'
packages.rtn <- sapply(cran.pkgs, get.package, cran.repos)



##-
##- 2.
##- Install GitHub packages (these versions are currently required)
##-
devtools::install_github("ramnathv/rCharts")
# devtools::install_github("trestletech/shinyRGL")
devtools::install_github("rstudio/DT")

##- For debug mode
source('config.r')
if(configuration$debug) {
   packages.rtn <- c(packages.rtn, get.package('pryr', cran.repos)) 
   devtools::install_github("vnijs/radiant")
   devtools::install_github("trestletech/shinyAce")
}
 
## N.B. 
##   The current Version of rgl and shinyRGL are not compatible.
##   The work around is to use latest rgl version >= 0.96 
##   with options(rgl.useNULL=TRUE) and not use shinyRGL at all.
##   This may have issues w/ blank rgl.snapshot / rgl.postscript.
##   In this case the work around is the old rgl version 0.93 
##   (or the one on trestletech GitHib as installed above) with shinyRGL.
##   Unfortunately, this version does not install on any Mac I have tried 
##    and the binary version segfaults with memory not mapped. 
##    See also this thread: 
##    https://groups.google.com/forum/#!topic/shiny-discuss/GJQ6tJNU7jU
#     install.packages("rgl") 
#     devtools::install_github("trestletech/rgl", ref="js-class")

## Works on server: rgl_0.93.963



##-
##- 3.
##- Check on missing utility programs (e.g. dssp and muscle)
##-
utilities <- c("muscle") #, "dssp", "stride", "mustang", "makeup")
missing.util <- nchar(Sys.which(utilities)) == 0
if( any(missing.util) ) {
   warning(paste0("  Checking for external utility programs failed\n",
      "    Please make sure '", paste(names(missing.util[missing.util]), collapse="', '"),
      "' is in your search path, see:\n",
      "    http://thegrantlab.org/bio3d/tutorials/installing-bio3d#utilities"))
} else {
   cat("External utility programs found\n")
}
