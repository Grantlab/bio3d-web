CREATE DATABASE shiny;
USE shiny;

CREATE TABLE IF NOT EXISTS `pdb_annotation` (
  `id` int(10) unsigned NOT NULL COMMENT 'Primary Key',
  `acc` varchar(12) NOT NULL,
  `structureId` varchar(4) NOT NULL,
  `chainId` varchar(2) NOT NULL,
  `source` varchar(30) NOT NULL,
  `experimentalTechnique` varchar(30) NOT NULL,
  `resolution` varchar(5) NOT NULL,
  `spaceGroup` varchar(20) NOT NULL,
  `ligandId` varchar(30) NOT NULL,
  `ligandName` varchar(30) NOT NULL,
  `compound` varchar(120) NOT NULL,
  `db_id` varchar(30) NOT NULL,
  `chainLength` int(11) NOT NULL,
  `sequence` text NOT NULL,
  `citation` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `pdb_annotation`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `pdb_annotation`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `pfam` (
  `id` int(10) unsigned NOT NULL COMMENT 'Primary Key',
  `acc` varchar(12) NOT NULL,
  `structureId` varchar(4) NOT NULL,
  `chainId` varchar(2) NOT NULL,
  `pdbResNumStart` int(10) NOT NULL,
  `pdbResNumEnd` int(10) NOT NULL,
  `pfamAcc` varchar(30) NOT NULL,
  `pfamName` varchar(30) NOT NULL,
  `pfamDesc` varchar(50) NOT NULL,
  `eValue` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `pfam`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `pfam`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',AUTO_INCREMENT=1;


