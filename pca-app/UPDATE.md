# Update PCA-app on DCMB VM #

The general steps to update the PCA-app on DCMB VM are as follows:

1. Pull from Bio3D bitbucket repository.
2. Copy over the new files to the Shiny Server app directory
3. Reload the Shiny Server

## Pull from repository ##

```bash
# Connect to the server, if not done so already
ssh user@dcmb-grant-shiny.umms.med.umich.edu
cd /nfs/DCMB-bjgrant-shiny-data/bio3d

# Ensure you are on branch feature_shiny
git branch
sudo git checkout feature_shiny
# sudo git stash
sudo git pull
# sudo git stash pop
```

## Copy files to Shiny Server app dir ##

```sh
# Do not forget the trailing '/'
sudo rsync -av --exclude 'config.r*' --exclude 'log' /nfs/DCMB-bjgrant-shiny-data/bio3d/shiny_devel/pca-app/ /srv/shiny-server/pca-app/
```

## Reload the Shiny Server ##

```sh
# A restart is typically not required,
# can be run as below after building R packages
# sudo restart shiny-server
sudo reload shiny-server
```

The changes will not affect open connections (browser sessions already running). To see the effect, just click refresh in your browser. New connections will automatically have the updated app.


## Contact ##

For any issues with the PCA-app, you are welcome to:

* Submit suggestions and bug-reports at: https://bitbucket.org/Grantlab/bio3d/issues
* Compose a friendly e-mail to: bjgrant@umich.edu or jari@umich.edu
