
## first 4 chars should be upper
## any chainId should remain untouched - see e.g. PDB ID 3R1C
format_pdbids <- function(acc, casefmt=toupper) {
  mysplit <- function(x) {
    str <- unlist(strsplit(x, "_"))
    if(length(str)>1)
      paste(casefmt(str[1]), "_", str[2], sep="")
    else
      casefmt(str[1])
  }

  out <- unlist(lapply(acc, mysplit))
  return(out)
}

## pdb2lum_A.ent.gz --> 2lum_A
## note that chain ID could be longer than 1 char
pdbfilename2label <- function(fn) {
  fn <- basename(fn)
  parsefn <- function(x) {
    a <- unlist(strsplit(x, "_"))
    b <- unlist(strsplit(a, "\\."))
    paste(substr(a[1], 4, 7), b[2], sep="_")
  }
  return(unlist(lapply(fn, parsefn)))
}

##- Remove leading and trailing spaces from character strings
trim.character <- function(s) {
  s <- sub("^ +", "", s)
  s <- sub(" +$", "", s)
  s[(s=="")]<-""
  return(s)
}

##- generates a random string, e.g. 11ca4f6ea112
randstr <- function() {
  return(basename(tempfile(pattern="")))
}

'col2hex' <- function(cname) {
    colMat <- col2rgb(cname)
    rgb(red = colMat[1, ]/255,
        green = colMat[2, ]/255,
        blue = colMat[3, ]/255)
}

## hacked version summary.pdb()
pdbsum <- function(object, printseq=FALSE, pdbid = NULL, chainid = NULL, ...) {

  ## Print a summary of basic PDB object features

  if( !is.pdb(object) ) {
    stop("Input should be a pdb object, as obtained from 'read.pdb()'")
  }

  ## Multi-model check and total atom count
  nmodel <- nrow(object$xyz)
  if( is.null(nmodel) ) {
    ntotal <- length(object$xyz)/3
    nmodel = 1
  } else {
    ntotal <- length(object$xyz[1,])/3
  }

  nxyz <- length(object$xyz)
  nres <- sum(object$calpha)
  chains <- unique(object$atom[,"chain"])

  all.inds <- atom.select(object, "all", verbose=FALSE)$atom
  prot.inds <- atom.select(object, "protein", verbose=FALSE)$atom
  nuc.inds <- atom.select(object, "nucleic", verbose=FALSE)$atom
  other.inds <- all.inds[! (all.inds %in% c(prot.inds, nuc.inds)) ]
  
  nprot <-length(prot.inds)
  nnuc <-length(nuc.inds)
  nresnuc <- length(unique(
    paste(object$atom$chain[nuc.inds], object$atom$insert[nuc.inds], object$atom$resno[nuc.inds], sep="-")))
                                                
  het <- object$atom[other.inds,]
  nhet.atom <- nrow(het)
  
  if(is.null(nhet.atom) | nhet.atom==0) {
    nhet.atom <- 0
    nhet.res <- 0
    hetres <- "none"
  } else { 
  	hetres.resno <- apply(het[,c("chain","resno","resid")], 1, paste, collapse=".")
  	nhet.res <- length(unique(hetres.resno))
	hetres.nres <- table(het[,c("resid")][!duplicated(hetres.resno)])
  	hetres <- paste( paste0( names(hetres.nres), " (",hetres.nres, ")"), collapse=", ")
  }

  if((nprot+nnuc+nhet.atom) != ntotal)
    warning("nPROTEIN + nNUCLEIC + nNON-PROTEIN + nNON-NUCLEIC != nTotal")

 
  #cat("\n Call:  ", paste(deparse(object$call), sep = "\n", collapse = "\n"),
                                        #      "\n", sep = "")

  #cat("\n Call: ", 
  #    "\n   pdb <- ", "read.pdb('", pdbid, "')",
  #    "\n   pdb <- ", "trim(pdb, chain = '", chainid, "')",
  #    "\n", sep = "")
  pdb_read_log <- NULL
  pdb_read_log <- paste0(pdb_read_log, "\n RCSB PDB ID:", pdbid)
  pdb_read_log <- paste0(pdb_read_log, "\n Selected Chain ID:", chainid,
      "\n")
  
  s <- paste0("\n Total Models#: ", nmodel, 
              "\n Total Atoms#: ", ntotal, ##",  XYZs#: ", nxyz,
              
              "\n Chains#: ", length(chains),
              "  (values: ", paste(chains, collapse=" "),")",

              "\n\n Protein Atoms#: ", nprot,
              "\n  (residues/Calpha atoms#: ", nres,")",
              
              "\n Nucleic acid Atoms#: ", nnuc,
              "\n  (residues/phosphate atoms#: ", nresnuc,")",

             "\n\n Non-protein/nucleic Atoms#: ", nhet.atom,
             " \n  (residues: ", nhet.res, ")",
             "\n Non-protein/nucleic resid values: \n  [ ", hetres," ]",
             "\n\n")
              
  #cat(s)
  pdb_read_log <- paste0(pdb_read_log, s)

  if(printseq) {
    ##protein
    if(nres>0) {
      prot.pdb <- trim.pdb(object, as.select(prot.inds))
      aa <- pdbseq(prot.pdb)
      if(!is.null(aa)) {
        if(nres > 225) {
          ## Trim long sequences before output
          aa <- c(aa[1:225], "...<cut>...", aa[(nres-3):nres])
        }
        aa <- paste("     ",  gsub(" ","", 
                                   strwrap( paste(aa,collapse=" "), 
                                           width=120, exdent=0) ), collapse="\n")
        cat("   Protein sequence:\n", aa, "\n\n", sep="")
      }
    }
    
    ## nucleic
    if(nresnuc>0) {
      na.pdb <- trim.pdb(object, as.select(nuc.inds))
      aa <- paste(object$atom$chain[nuc.inds], object$atom$insert[nuc.inds],
                  object$atom$resno[nuc.inds], object$atom$resid[nuc.inds],
                  sep="-")
      aa <- aa[!duplicated(aa)]
      aa <- unlist(lapply(strsplit(aa, "-"), function(x) x[4]))
      aa <- .aa321.na(aa)
      if(nresnuc > 225) {
        ## Trim long sequences before output
        aa <- c(aa[1:225], "...<cut>...", aa[(nresnuc-3):nresnuc])
      }
      aa <- paste("     ",  gsub(" ","", 
                                 strwrap( paste(aa,collapse=" "), 
                                         width=120, exdent=0) ), collapse="\n")
      cat("   Nucleic acid sequence:\n", aa, "\n\n", sep="")
    }
  }
    
  #i <- paste( attributes(object)$names, collapse=", ")
  #cat(strwrap(paste(" + attr:",i,"\n"),width=45, exdent=8), sep="\n")

  #invisible( c(nmodel=nmodel, natom=ntotal, nxyz=nxyz, nchains=length(chains),  
  #             nprot=nprot, nprot.res=nres, nother=nhet.atom, nother.res=nhet.res) )
  pdb_read_log


}


mk.pfam.tbl <- function(aa1) {
  ##-- Annotate a sequence with PFAM (online)
  ##    Used for single sequence input annotation.
  ##
  ## pdb <- read.pdb('5p21')
  ## aa <- pdbseq(pdb)
  ## mk.pfam.tbl(aa)

  ##- Check for invalid residue types
  aa.protein <- c("-", "X", "A", "C", "D", "E", "F", "G", "H", "I", "K", 
                  "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "Y")
  aa1.match <- aa1 %in% aa.protein
  aa1.valid <- ifelse(all(aa1.match), "YES", "NO")

  ##- Report on top PFAM match (with link)
  fam <- hmmer(seq=aa1, type="hmmscan", db="pfam")
  fam.txt <- paste0(fam[1,"name"], " (acc: ", fam[1,"acc"],")")
  fam.url <- paste0("http://pfam.sanger.ac.uk/family/",fam[1,"acc"])

  ##- Simple length check and exclusionn of long sequences
  aa1.len <- length(aa1)
  #if(aa1.len > 500) { stop("Sequence length beyond our limts!") }

  return( c("length"=aa1.len, "valid"=aa1.valid, "pfam"=fam.txt, 
    "pfam.url"=fam.url, "pfam.e"=fam[1,"evalue"]) )
}


vec_is_sorted <- function(v) {
  return(sum(sort(v) == v) == length(v))
}

split_height <- function(hc) {
  i <- which.max(diff(hc$height))
  split_height <- (hc$height[i] * 0.7 + hc$height[i+1] * 0.3)
  return(split_height)
}

cutreeBio3d <- function(hc, minDistance=0.1, k=NA) {
  sh <- split_height(hc)

  message(paste("k is", k))

  if(vec_is_sorted(hc$height) && max(diff(hc$height)) >= minDistance) {
    c <- stats::cutree(hc, h=sh)
  }
  else {
    c <- rep(1, length(hc$order))
  }

  autok <- length(unique(c))

  if(!is.na(k)) {
    c <- stats::cutree(hc, k=k)
  }

  k <- length(unique(c))

  return(list(grps=c, h=sh, k=k, autok=autok))
}


## revised version of filter.rmsd()
filter.mat <- function(mat, dist.fun=as.dist, cutoff = 0.5, method = "complete") {

  dist <- dist.fun(mat)
  tree <- hclust(dist, method = method)

  h <- cutoff
  n <- nrow(tree$merge) + 1
  k <- integer(length(h))
  k <- n + 1 - apply(outer(c(tree$height, Inf), h, ">"), 2,
                     which.max)

  ans <- as.vector(cutree(tree, k))
  cluster.rep <- NULL
  for (i in 1:k) {
    ind <- which(ans == i)
    if (length(ind) == 1) {
      cluster.rep <- c(cluster.rep, ind)
    }
    else {
      cluster.rep <- c(cluster.rep,
                       ind[which.min(colSums(mat[ind, ind]))])
    }
  }
  return(list(ind = cluster.rep, tree = tree))
  
}

##- wrapper for shinyjs (en)disable Bootstrap elements
#   ...: id/selector passed to shinyjs
#   id: html element id
#   selector: jquery selector
toggle.tab <- function(tabs = NULL, enable = TRUE) {
    if(!is.null(tabs)) {
        sel.parent <- paste0("#navbarpage li:has(> ",
                           paste0("a[data-value='", tabs, "']", collapse = ','), ')')
        sel.child <- paste0("#navbarpage li ",
                          paste0("a[data-value='", tabs, "']", collapse = ','))

        if(enable) {
            js$enableTab(sel.child)
            #shinyjs::removeClass(selector = sel.parent, class = 'disabled')
            #shinyjs::enable(selector = sel.parent)
            #shinyjs::enable(selector = sel.child)
        }
        else {
            js$disableTab(sel.child)
            #shinyjs::addClass(selector = sel.parent, class = 'disabled')
            #shinyjs::disable(selector = sel.parent)
        }
    }
}


hook_webgl = function(before, options, envir) {
  #library(rgl)
  ## after a chunk has been evaluated
  if (before || rgl.cur() == 0) return()  # no active device
  name <- tempfile("webgl", tmpdir = ".", fileext = ".html")
  #on.exit(unlink(name))
  message(name)
  retina <- options$fig.retina
  if (!is.numeric(retina))
      retina <- 1
  dpi <- options$dpi/retina
  margin <- options$rgl.margin
  if (is.null(margin))
      margin <- 100
  par3d(windowRect = margin + dpi * c(0, 0, options$fig.width, options$fig.height))
  #par3d(windowRect = 100 + options$dpi * c(0, 0, options$fig.width, options$fig.height))
  Sys.sleep(.05) # need time to respond to window size change

  prefix <- gsub("[^[:alnum:]]", "_", options$label)
  prefix <- sub("^([^[:alpha:]])", "_\\1", prefix)
  writeLines(
    c(paste0('%', prefix, 'WebGL%')
        ,'<script>webGLStart();</script>'
    ),
    tpl <- tempfile())
  message(tpl)
  message(paste0('spanshot: ', !rgl.useNULL()))
  outname <- writeWebGL(dir = dirname(name), filename = name, snapshot = !rgl.useNULL(), template = tpl, prefix = prefix)
  res = readLines(name)
  res = res[!grepl("^\\s*$", res)] # remove blank lines
  paste(gsub("^\\s+", "", res), collapse = "\n")
  #outname = writeWebGL(dir = dirname(name), filename = name, template = tpl, snapshot = FALSE)
  #message(outname)
  #res = readLines(name)
  #res = res[!grepl('^\\s*$', res)] # remove blank lines
  #paste(gsub('^\\s*<', '<', res), collapse = '\n') # no spaces before HTML tags
}
