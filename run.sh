#!/bin/bash

# Directories on the host
export DATADIR=/tmp/shinydata
export LOGDIR=/tmp/shinylogs

# Build image 
docker build . -t bio3d/bio3d-web:devel -f Dockerfile 

# Run temporary container - expose directories on the host to the container
docker run --rm -p 3838:3838 \
    -v $DATADIR:/net/shiny_data/ \
    -v $LOGDIR:/var/log/shiny-server/ \
    bio3d/bio3d-web:devel

